<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ImageGallery;
use Illuminate\Support\Facades\Auth;


class ImageGalleryController extends Controller
{
    //

    public function index()
    {
    	$images = ImageGallery::get();
        if(Auth::user()) {
            return view('modules.gallery.gallery',compact('images'));
        } else {
    	    return view('gallery',compact('images'));
        }
    }

    public function create() {
        return view('modules.gallery.image-gallery');
    }

    public function upload(Request $request)
    {
    	$this->validate($request, [
    		'title' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);


        $input['image'] = time().'.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path('images/gallery'), $input['image']);


        $input['title'] = $request->title;
        ImageGallery::create($input);


    	return back()
    		->with('success','Image Uploaded successfully.');
    }
}
