<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ImageGallery;
use App\Models\Tariff;
use App\Models\Inquiry;
use App\Models\Contact;
use App\Mail\CustomerInquiry;
use App\Mail\ContactUs;
use Illuminate\Support\Facades\Mail;
class IndexController extends Controller
{
    //
    public function index(){
        $images = ImageGallery::get();
    	// return view('image-gallery',compact('images'));
        return view('index', compact('images'));
    }

    public function contactus() {
        return view('contactus');
    }

    public function explore(){
        return view('explore');
    }

    public function tariff() {
        $tariffs = Tariff::where('visibility', 1)->get();
        return view('tariff', compact('tariffs'));
    }

    public function bookingInquery($t_id) {
        $tariffs = Tariff::where('id', $t_id)->get();
        return view('inquiry', compact('tariffs', 't_id'));
    }

    public function bookingInquerySave(Request $request, $t_id) {
        // save data
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'checkindate' => 'required|before:checkoutdate',
            'checkoutdate' => 'required',
            'adult' => 'required',
            'children' => 'required',
            'noofrooms' => 'required',
            'message' => '',
            'g-recaptcha-response' => 'required|captcha'
        ]);
        $userEmail = $request->email;
        $customerDetails = [
        'name' => $request->name,
        'email' => $request->email,
        'phone' => $request->phone,
        'checkindate' => $request->checkindate,
        'checkoutdate' => $request->checkoutdate,
        'adult' => $request->adult,
        'children' => $request->children,
        'noofrooms' => $request->noofrooms,
        'message' => $request->message];
        $roomDetails = Tariff::where('id', $t_id)->first();
        $inquiryData = Inquiry::create($request->all());
        $maildata1 = Mail::to($userEmail)->queue(new CustomerInquiry($customerDetails, $roomDetails));
        return redirect()->route('thankyou');
    }

    public function savecontactUsData(Request $request) {
        $userEmail = $request->email;

        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'message' => 'required',
            'g-recaptcha-response' => 'required|captcha'
        ]);
        $customerDetails = [
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'message' => $request->message ];

        $contactData = Contact::create($request->all());
        $customerMails = Mail::to($userEmail)->queue(new ContactUs($customerDetails));
        return redirect()->route('thankyou');
    }

    public function reloadCaptcha()
    {
        return response()->json(['captcha'=> captcha_img()]);
    }

}
