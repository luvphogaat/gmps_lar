<?php

namespace App\Http\Controllers;

use App\Models\Stock;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $stockList = Stock::get();
        return view('modules.stocks.index' , compact('stockList'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $stockList = Stock::get();
        return view('modules.stocks.addstock' , compact('stockList'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd($request);
        // Str::ucfirst($testString);
        Stock::create([
            'stock_name' => Str::ucfirst($request->stock_name),
            'description' => 'Added Stock',
            'brand' => Str::ucfirst($request->brand),
            'price' => $request->price,
            'quantity' => $request->quantity,
            'alert_stock' => '10'
        ]);
        return redirect()->route('stocks.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function show(Stock $stock)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function edit(Stock $stock)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Stock $stock)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function destroy(Stock $stock)
    {
        //
    }

    public function autoComplete(Request $request) {
        $query = $request->get('term','');

        $products=Stock::where('stock_name','LIKE','%'.$query.'%')->get();

        $data=array();
        foreach ($products as $product) {
                $data[]=array('value'=>$product->stock_name,'id'=>$product->id);
        }
        if(count($data))
             return $data;
        else
            return ['value'=>'','id'=>''];
    }
}
