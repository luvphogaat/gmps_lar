<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// use paytm\checksum\PaytmChecksumLibrary;
// use PaytmChecksum;
// require_once("./PaytmChecksum.php");
use PaytmWallet;
require base_path() . '/vendor/anandsiddharth/laravel-paytm-wallet/lib/encdec_paytm.php';
use Illuminate\Support\Facades\Http;


class BookingController extends Controller
{
    //


    public function index() {
        // $this->sampleAPI();

        return view('booking.booking-engine');
    }

    public function userDetails(Request $request) {
        $checkInDate = '';
        $checkOutDate = '';
        $request->session()->put('userData', $request);

    }

    public function paytmPayment(Request $request)
    {
        $payment = PaytmWallet::with('receive');
        $payment->prepare([
          'order' => rand(),
          'user' => rand(10,1000),
          'mobile_number' => '8447473921',
          'email' => 'paytmtest@gmail.com',
          'amount' => '11.00',
          'callback_url' => route('paytm.callback'),
        ]);
        return $payment->receive();
    }

    public function paytmCallback()
    {
        $transaction = PaytmWallet::with('receive');

        $response = $transaction->response(); // To get raw response as array
        //Check out response parameters sent by paytm here -> http://paywithpaytm.com/developer/paytm_api_doc?target=interpreting-response-sent-by-paytm
        dd($response);
        if($transaction->isSuccessful()){
          //Transaction Successful
          return view('booking.paytm-success');
        }else if($transaction->isFailed()){
          //Transaction Failed
          return view('booking.paytm-fail');
        }else if($transaction->isOpen()){
          //Transaction Open/Processing
          return view('booking.paytm-fail');
        }
        $transaction->getResponseMessage(); //Get Response Message If Available
        //get important parameters via public methods
        $transaction->getOrderId(); // Get order id
        $transaction->getTransactionId(); // Get transaction id
    }

    public function sampleAPI() {
        $params = [
			'REQUEST_TYPE' => 'DEFAULT',
			'MID' => 'InUvFt35372539186620',
			'ORDER_ID' => 'GC21021212',
			'CUST_ID' => '12121',
			'INDUSTRY_TYPE_ID' => 'Retail',
			'CHANNEL_ID' => 'WEB',
			'TXN_AMOUNT' => '21.50',
			'WEBSITE' => 'WEBSTAGING',
            'CALLBACK_URL' => 'http://localhost:8002/book/paytm-callback',
            'MOBILE_NO' => '8447473921',
            'EMAIL' => 'luv.phogaat@gmail.com',
		];

        $apiURL = 'https://securegw-stage.paytm.in/paymentservices/qr/create';

        // POST Data
        $postInput = [
            "mid"             => "InUvFt35372539186620",
            "orderId"       => "OREDRID98765",
            "amount"          => "20.50",
            "businessType"  => "UPI_QR_CODE",
            "posId"         => "S12_123",
            "displayName"   => "Ganga Cottage"
        ];
        // dd(getChecksumFromArray($postInput, 'b%eN2sFc2J#k54He'));


        // Headers
        $headers = [
            "clientId"	        => 'C11',
            "version"	        => 'v1',
            "channelId"         => "WEB",
            'signature' => getChecksumFromArray($postInput, 'b%eN2sFc2J#k54He'),
        ];

        $response = Http::withHeaders($headers)->post($apiURL, $postInput);

        $statusCode = $response->status();
        $responseBody = json_decode($response->getBody(), true);

        dd($responseBody); // body response
    }
}
