<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CompanyInfo;

class CompanyInfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $companyinfo = CompanyInfo::where('id', 1)->first();
        return view('modules.profile.company-info', compact('companyinfo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $info = $request->all();
        // dd($request);
        $companyinfo = CompanyInfo::find($id);
        // $companyinfo->update($request->all());
        $companyinfo->company_name = $info['company_name'];
        $companyinfo->gst = $info['gst'];
        $companyinfo->cgst = $info['cgst'];
        $companyinfo->sgst = $info['sgst'];
        $companyinfo->address = $info['address'];
        $companyinfo->phone = $info['phone'];
        $companyinfo->state = $info['state'];
        $companyinfo->country = $info['country'];
        $companyinfo->message = $info['message'];
        $companyinfo->currency = $info['currency'];
        $companyinfo->save();
        return redirect()->route('company-info.index')
            ->with('success', 'Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
