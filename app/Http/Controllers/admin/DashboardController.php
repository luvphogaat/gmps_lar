<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Products;

class DashboardController extends Controller
{
    //
    public function index() {
        // return view('dashboard');
        // if(Auth::user()->hasRole('user')) {
        //     return view('');
        // } else
        if(Auth::user()->hasRole('staff')) {
            return view('modules.dashboard.dashboardStaff');
        } elseif(Auth::user()->hasRole('admin')){
            return view('modules.dashboard.dashboard');
        } elseif(Auth::user()->hasRole('manager')) {
            return view('modules.dashboard.dashboardManager');
        }
    }

    public function myprofile() {
        return view('modules.profile.myprofile');
    }

    public function companyinfo() {

    }

    public function productlist() {
        $products = Products::paginate(15);
        return view('modules.products.index', compact('products'));
    }
}
