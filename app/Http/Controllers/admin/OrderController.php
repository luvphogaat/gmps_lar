<?php


namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;

use App\Models\Order;
use App\Models\Products;
use App\Models\Rooms;
use App\Models\Order_Detail;
use App\Models\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use \NumberFormatter;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('modules.order.index');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $order_number = Order::all()->last()->id + 1;
        return view('modules.order.neworder', compact('order_number'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // return $request->all();

        DB::transaction(function() use ($request){
            // Order Model
            $order = new Order;
            $order->name = 'Luv Phogaat';
            $order->phone = '8447473921';
            $order->table_id = $request->table_id;
            $order->pay_status = 'pending';
            $order->pay_id = '';
            $order->order_type = 'ROOM';
            $order->save();
            $order_id = $order->id;

            // Order Details Modal
            for ($product = 0; $product < count($request->product_id); $product++){
                $order_details = new Order_Detail;
                $order_details->order_id = $order_id;
                $order_details->product_id = $request->product_id[$product];
                $order_details->quantity = $request->quantity[$product] ? $request->quantity[$product] : 1;
                $order_details->unitprice = $request->price[$product];
                $order_details->amount = $request->total_amount[$product];
                $order_details->save();
            }
            Cart::truncate();
        });
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show($orderId)
    {
        return view('modules.order.singleorder', compact('orderId'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
        // dd($request, $order);
        $order->fill($request->only([
            'pay_status'
        ]));
        $order->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }

    public function listOrder() {

        // $lastId = Order_Detail::max('order_id');
        // $order_receipt = Order_Detail::where('order_id', $lastId)->get();
        // $totalPrice = $this->totalAmount($order_receipt);
        // return view('modules.order.list');
    }

}
