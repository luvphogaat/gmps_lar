<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use LaravelDaily\Invoices\Invoice;
use LaravelDaily\Invoices\Classes\Buyer;
use LaravelDaily\Invoices\Classes\Party;
use LaravelDaily\Invoices\Classes\InvoiceItem;
use App\Models\Order;
use App\Models\Reservation;
use App\Models\Rooms;
use App\Models\GuestDetails;
use PDF;
use QrCode;

class RecieptController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Printing Food Receipts
        // {{ dd('hello'); }}
            $orderReceipt = Order::with('orderdetail.product')->where('id', $id)->first();
            $orderReceiptCount = count($orderReceipt->orderdetail);
            $qrCode = QrCode::size(300)->generate('https://docs.google.com/forms/d/e/1FAIpQLSeBuLYsu26nApRfDx-AIaQYhcTfjBmJASaJzC2dXjDGwCKduw/viewform?usp=sf_link');
            $paidpath = 'https://www.pngarts.com/files/2/Paid-Free-PNG-Image.png';
            $type = pathinfo($paidpath, PATHINFO_EXTENSION);
            $data = file_get_contents($paidpath);
            $paidbase64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
            view()->share('orderReceipt',[$orderReceipt, $qrCode, $paidbase64]);
            $dompdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('modules.order.foodreceipt', $orderReceipt);
            $dompdf->setOptions(['defaultFont' => 'san-serif', 'dpi' => 200]);
            $dompdf->setPaper(array(0,0,210,620 + 10 * $orderReceiptCount), 'portrait'  );
            // Output the generated PDF to Browser
            return $dompdf->stream();
            // return view('modules.order.foodreceipt', compact('orderReceipt'));

        // Old PDF
        // dd($orderReceipt);
        // $customer = new Party([
        //     'name'          => $orderReceipt->name,
        //     'phone'       => $orderReceipt->phone,
        //     'custom_fields' => [
        //         'email'         => 'luv.phogaat@gmail.com',
        //         'address'       => 'The Green Street 12',
        //     ],
        // ]);

        // $items = [];

        // foreach ($orderReceipt->orderdetail as $key1 => $value1) {
        //     array_push($items,
        //         (new InvoiceItem())->title($value1->product->name)->pricePerUnit($value1->unitprice)->quantity($value1->quantity)                );
        // }

        // $notes = '';

        // $invoice = Invoice::make('Invoice')
        //     ->status(__($orderReceipt->pay_status == 'paid' ? 'invoices::invoice.paid' : 'invoices::invoice.due'))
        //     ->sequence($orderReceipt->id)
        //     ->serialNumberFormat('{SERIES}/{SEQUENCE}')
        //     ->buyer($customer)
        //     ->date(now()->subWeeks(3))
        //     ->filename($customer->name . '-' .$orderReceipt->id . '-' . date('d-m-Y-h-i-sa'))
        //     ->addItems($items)
        //     ->notes($notes)
        //     ->taxRate(12)
        //     // ->discountByPercent(4)
        //     ->logo('https://www.gangacottage.com/images/logo_new/GC_New_1.png')
        //     // You can additionally save generated invoice to configured disk
        //     // ->save('pdf')
        //     ;

        // $link = $invoice->url();
        // // dd($link);
        // // Then send email to party with link
        //     // dd($link);
        // // And return invoice itself to browser or have a different view
        // //  #watermark {
        // //     position: fixed;

        // //     /**
        // //         Set a position in the page for your image
        // //         This should center it vertically
        // //     **/
        // //     bottom:   10cm;
        // //     left:     5.5cm;

        // //     /** Change image dimensions**/
        // //     width:    8cm;
        // //     height:   8cm;

        // //     /** Your watermark should be behind every content**/
        // //     z-index:  -1000;
        // // }
        // return $invoice->stream();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // Rooms Controller

    public function roomshow($id)
    {
        //
        $orderReceipt = Reservation::with(['room'])->where('reservation_number', $id)->get();
        $guestId = $orderReceipt[0]->guestId;
        $amount_paid = Reservation::with(['room'])->where('reservation_number', $id)->sum('paid_amount');
        $amount_total = Reservation::with(['room'])->where('reservation_number', $id)->sum('total_amount');
        // $this->currencyFormatter($order_details->orderdetail->sum('amount'))
        $guestDetails = GuestDetails::where('id', $guestId)->first();
        // dd($orderReceipt);
        $customer = new Party([
            'name'          => $guestDetails->name,
            'phone'       => $guestDetails->contact,
            'custom_fields' => [
                'email'         => $guestDetails->email,
            ],
        ]);

        $items = [];
        foreach ($orderReceipt as $key1 => $value1) {
            array_push($items,
                (new InvoiceItem())->title($value1->room[0]->room_type)->pricePerUnit($value1->sell_amount)->quantity($value1->nights));
        }

        $notes = '';

        $invoice = Invoice::make('Invoice')
            // ->series('GC')
            // ability to include translated invoice status
            // in case it was paid
            ->make('receipt')->template('roomreceipt')
            ->status(__($amount_paid == $amount_total ? 'invoices::invoice.paid' : 'invoices::invoice.due'))
            ->sequence($id)
            ->serialNumberFormat('{SERIES}/{SEQUENCE}')
            ->buyer($customer)
            ->date(now()->subWeeks(3))
            ->filename($customer->name)
            ->addItems($items)
            ->notes($notes)
            ->taxRate(12)
            ->discountByPercent(0)
            ->logo('https://www.gangacottage.com/images/logo_new/GC_New_1.png');
            // You can additionally save generated invoice to configured disk
            // ->save('public');

        $link = $invoice->url();
        // Then send email to party with link
            // dd($link);
        // And return invoice itself to browser or have a different view
        return $invoice->stream();
    }

    public static function currencyFormatter($amount) {
        $formater =  new \NumberFormatter('en_IN', \NumberFormatter::CURRENCY);
        return $formater->format($amount);
    }

    public static function getAmountInWords(float $amount, ?string $locale = null)
    {
        $amount    = number_format($amount, 2, '.', '');
        $formatter = new \NumberFormatter('en_IN', \NumberFormatter::SPELLOUT);

        $value = explode('.', $amount);

        $integer_value  = (int) $value[0] !== 0 ? $formatter->format($value[0]) : 0;
        $fraction_value = isset($value[1]) ? $formatter->format($value[1]) : 0;
        // dd($value);
        if ($value[1] <= 0) {
            return sprintf('%s %s only', ucfirst($integer_value), strtoupper(''));
        }

        return sprintf(
            trans('invoices::invoice.amount_in_words_format'),
            ucfirst($integer_value),
            strtoupper(''),
            $fraction_value,
            ''
        );
    }
}
