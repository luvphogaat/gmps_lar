<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Rooms;
use App\Models\GuestDetails;
use App\Models\Reservation;
use PDF;

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $reservationList = Reservation::with(['room', 'guest'])->orderBy('created_at', 'desc')->get();
        return view('modules.reservation.index', compact('reservationList'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // create reservation and pdf for stating reservation
        $roomList = Rooms::get();
        return view('modules.reservation.make-reservation', compact('roomList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        DB::transaction(function () use ($request) {
            $reservation_number = Reservation::all()->last()->reservation_number + 1;
            foreach ($request->get('room') as $key => $value) {
            // dd($request->room, $request->get('room'), $value);
                $guestExist = GuestDetails::where('email', $request->email[$key])->first();
                if ($guestExist == null){
                    $userNewDetails = GuestDetails::create([
                        'name'      => $request->name[$key],
                        'email'     => $request->email[$key],
                        'contact'   => $request->phone[$key]
                    ]);
                    Reservation::create([
                        'reservation_number'=> $reservation_number,
                        'guestId'           => $userNewDetails->id,
                        'roomId'            => $value,
                        'adultCount'        => $request->adultCount[$key],
                        'childCount'        => $request->childCount[$key],
                        'checkin'           => $request->checkin[$key],
                        'checkout'          => $request->checkout[$key],
                        'nights'            => ceil(abs(strtotime($request->checkin[$key]) - strtotime($request->checkout[$key]))/ 86400),
                        'advance_amount'    => $request->advance_amount[$key],
                        'source'            => $request->source[$key],
                        'total_amount'      => $request->total_amount[$key],
                        'sell_amount'       => $request->sell_amount[$key],
                        'stay_status'       => 'Pending',
                        'paid_amount'       => $request->advance_amount[$key],
                    ]);
                } else {
                    Reservation::create([
                        'reservation_number'=> $reservation_number,
                        'guestId'           => $guestExist->id,
                        'roomId'            => $value,
                        'adultCount'        => $request->adultCount[$key],
                        'childCount'        => $request->childCount[$key],
                        'checkin'           => $request->checkin[$key],
                        'checkout'          => $request->checkout[$key],
                        'nights'            => ceil(abs(strtotime($request->checkin[$key]) - strtotime($request->checkout[$key]))/ 86400),
                        'advance_amount'    => $request->advance_amount[$key],
                        'source'            => $request->source[$key],
                        'total_amount'      => $request->total_amount[$key],
                        'sell_amount'       => $request->sell_amount[$key],
                        'stay_status'       => 'Pending',
                        'paid_amount'       => $request->advance_amount[$key],
                    ]);
                }
            }
        });
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $reservationList = Reservation::with(['room', 'guest'])->where('id', $id)->first();
        view()->share('reservationList',$reservationList);
        $dompdf = PDF::loadView('modules.reservation.view', $reservationList);

        // Output the generated PDF to Browser
        return $dompdf->stream();
        // return view('modules.reservation.view', compact('roomList'));
    }

    public function list() {
        $reservationList = Reservation::with(['room', 'guest'])->first();
        return view('modules.reservation.list', compact('reservationList'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($reservationId)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
