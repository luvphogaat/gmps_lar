<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Transaction;
// use paytm\checksum\PaytmChecksumLibrary;
// use PaytmChecksum;
require_once(__DIR__ . "/PaytmChecksum.php");

class TransactionController extends Controller
{
    //
    public function index() {
        $transactionList = Transaction::get();
        return view('modules.transaction.index', compact('transactionList'));
    }

    public function linkGenerate() {
        //
        $mid = 'InUvFt35372539186620';
        $mkey = 'b%eN2sFc2J#k54He';
        /* initialize an array */

        /* initialize an array */
        $paytmParams = array();

        /* add parameters in Array */
        $paytmParams["MID"] = $mid;
        $paytmParams["ORDERID"] = "GC202111301012";

        /**
        * Generate checksum by parameters we have
        * Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys
        */
        $paytmChecksum = PaytmChecksum::generateSignature($paytmParams, 'YOUR_MERCHANT_KEY');
        echo sprintf("generateSignature Returns: %s\n", $paytmChecksum);

        /* initialize JSON String */
        // $body = "{\"mid\":\"YOUR_MID_HERE\",\"orderId\":\"YOUR_ORDER_ID_HERE\"}";

        // /**
        // * Generate checksum by parameters we have in body
        // * Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys
        // */
        // $paytmChecksum = PaytmChecksum::generateSignature($body, 'YOUR_MERCHANT_KEY');
        // $verifySignature = PaytmChecksum::verifySignature($body, 'YOUR_MERCHANT_KEY', $paytmChecksum);
        // echo sprintf("generateSignature Returns: %s\n", $paytmChecksum);
        // echo sprintf("verifySignature Returns: %b\n\n", $verifySignature);
        //
        // return view('modules.transaction.create');
    }
}
