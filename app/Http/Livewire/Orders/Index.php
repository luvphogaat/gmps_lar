<?php

namespace App\Http\Livewire\Orders;

use Livewire\Component;
use App\Models\Order;
use App\Models\Products;

class Index extends Component
{
    public $orders, $products = [], $rooms = [], $product_code, $message, $productInCart, $totalPrice, $ordersCom;


    public function mount() {
        $this->products = Products::all();
        $this->orders = Order::with(['orderdetail', 'room'])->where('pay_status', 'pending')->get();
        $this->ordersCom = Order::with(['orderdetail', 'room'])->where('pay_status', 'paid')->orderBy('created_at', 'desc')->get();
    }

    public function currencyFormatter($amount) {
        $formater =  new \NumberFormatter($locale = 'en_IN', \NumberFormatter::CURRENCY);
        return $formater->format($amount);
    }

    public function totalAmount($values) {
        $totalPrice = 0.00;
        foreach ($values as $key => $value) {
            $totalPrice += ($value-> product_qty * $value->product_price);
        }
        $formatter = new \NumberFormatter($locale = 'en_IN', \NumberFormatter::CURRENCY);
        return  $formatter->format($totalPrice);
    }


    public function render()
    {
        return view('livewire.orders.index');
    }
}
