<?php

namespace App\Http\Livewire\Orders;

use Livewire\Component;
use App\Models\Products;
use App\Models\Rooms;
use App\Models\Cart;

class NewOrder extends Component
{
    public $orders, $products = [], $rooms = [], $product_code, $message, $productInCart, $totalPrice, $order_number, $pay_type ;


    public function mount($order_number) {
        $this->products = Products::orderBy('name', 'asc')->get();
        $this->rooms = Rooms::all();
        $this->productInCart = Cart::all();
        $this->totalPrice = $this->totalAmount($this->productInCart);
        $this->order_number = $order_number;
        $this->pay_type = 'pending';
        // dd($this->productInCart);
    }

    public function totalAmount($values) {
        $totalPrice = 0.00;
        foreach ($values as $key => $value) {
            $totalPrice += ($value-> product_qty * $value->product_price);
        }
        $formatter = new \NumberFormatter($locale = 'en_IN', \NumberFormatter::CURRENCY);
        return  $formatter->format($totalPrice);
    }

    public function InserttoCart() {
        $countProduct = Products::where('id', $this->product_code)->first();
        // dd($countProduct);
        $countCart = Cart::where('product_id', $this->product_code)->count();
        // if ($countCart > 0) {
        //     // increase the count
        //     $cartExistingProduct = Cart::where('product_id', $this->product_code)->first();
        //     $cartExistingProduct->product_qty = $cartExistingProduct->product_qty + 1;
        //     $cartExistingProduct->save();
        //     $this->productInCart = Cart::all();
        // }
        // else {
            $addToCart = new Cart;
            $addToCart->product_id = $countProduct->id;
            $addToCart->product_qty = 1;
            $addToCart->product_price = $countProduct->price;
            $addToCart->user_id = auth()->user()->id;
            $addToCart->save();
            $this->productInCart->prepend($addToCart);
        // }
        $this->product_code = '';
    }

    public function ChangeQty($orderDId,$eventvalue){
        if ( $eventvalue == 0 ) {
            $orderExistingProduct = Cart::where('id', $orderDId)->first();
            $orderExistingProduct->delete();
        } else {
            $orderExistingProduct = Cart::where('id', $orderDId)->first();
            $orderExistingProduct->product_qty = $eventvalue;
            $orderExistingProduct->save();
        }
        $this->productInCart = Cart::all();
    }

    public function removeProduct($cartId) {
        $deleteCart = Cart::find($cartId);
        $deleteCart->delete();
        $this->productInCart =  $this->productInCart->except($cartId);
    }

    public function currencyFormatter($amount) {
        $formater =  new \NumberFormatter($locale = 'en_IN', \NumberFormatter::CURRENCY);
        return $formater->format($amount);
    }


    public function render()
    {
        return view('livewire.orders.new-order');
    }
}
