<?php

namespace App\Http\Livewire\Orders;

use Livewire\Component;
use App\Models\Products;
use App\Models\Rooms;
use App\Models\Order;
use App\Models\Order_Detail;

class ViewSingle extends Component
{

    public $orders, $products = [], $rooms = [], $product_code, $message, $order_details, $totalPrice, $orderId, $qty_value, $pay_type ;


    public function mount($orderId) {
        // dd($orderId);
        $this->orderId = $orderId;
        $this->products = Products::all();
        $this->rooms = Rooms::all();
        $this->order_details = Order::with(['room', 'orderdetail'])->where('id', $orderId)->first();
        $this->totalPrice = 0.00;
        // dd($this->productInCart);
    }

    public function totalAmount($values) {
        $totalPrice = 0.00;
        foreach ($values as $key => $value) {
            $totalPrice += ($value-> product_qty * $value->product_price);
        }
        $formatter = new \NumberFormatter($locale = 'en_IN', \NumberFormatter::CURRENCY);
        return  $formatter->format($totalPrice);
    }

    public function InserttoOrder() {
        $selected_product = Products::where('id', $this->product_code)->first();
        $order_exist = Order_Detail::where('product_id', $this->product_code)->where('order_id', $this->orderId)->count();
        // if ($order_exist > 0) {
        //     // increase the count
        //     $orderExistingProduct = Order_Detail::where('product_id', $this->product_code)->where('order_id', $this->orderId)->first();
        //     $orderExistingProduct->quantity = $orderExistingProduct->quantity + 1;
        //     $orderExistingProduct->amount = $orderExistingProduct->quantity * $orderExistingProduct->unitprice;
        //     $orderExistingProduct->save();
        //     // dd($orderExistingProduct);
        //     $this->order_details = Order::with(['room', 'orderdetail'])->where('id', $this->orderId)->first();
        // }
        // else {
            $addToOrder = new Order_Detail;
            $addToOrder->order_id = $this->orderId;
            $addToOrder->product_id = $selected_product->id;
            $addToOrder->quantity = 1;
            $addToOrder->unitprice = $selected_product->price;
            $addToOrder->amount = $selected_product->price * 1;
            // $addToOrder->user_id = auth()->user()->id;
            $addToOrder->save();
            // $this->order_details = Order::with(['room', 'orderdetail'])->where('id', $this->orderId)->first();
            $this->order_details->prepend($addToOrder);
        // }
        $this->product_code = '';
    }

    public function ChangeQty($orderDId, $eventvalue){
        if ( $eventvalue == 0 ) {
            $orderExistingProduct = Order_Detail::where('id', $orderDId)->first();
            $orderExistingProduct->delete();
        } else {
            $orderExistingProduct = Order_Detail::where('id', $orderDId)->first();
            $orderExistingProduct->quantity = $eventvalue;
            $orderExistingProduct->amount = $orderExistingProduct->quantity * $orderExistingProduct->unitprice;
            $orderExistingProduct->save();
        }
        $this->order_details = Order::with(['room', 'orderdetail'])->where('id', $this->orderId)->first();

    }

    public function removeProduct($cartId) {
        $deleteCart = Order_Detail::find($cartId);
        $deleteCart->delete();
        $this->order_details =  $this->order_details->except($cartId);
    }

    public function currencyFormatter($amount) {
        $formater =  new \NumberFormatter($locale = 'en_IN', \NumberFormatter::CURRENCY);
        return $formater->format($amount);
    }


    public function render()
    {
        return view('livewire.orders.view-single');
    }
}
