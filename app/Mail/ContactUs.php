<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactUs extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $customerDetails;
    public function __construct($customerDetails)
    {
        //
        $this->customerDetails = $customerDetails;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no-reply@gangacottage.com', 'Ganga Cottage')
                ->subject('Thanks for reaching out to us ' . $this->customerDetails['name'])
                ->bcc('gangacottageranikhet@gmail.com')
                ->markdown('mails.contactus');
    }
}
