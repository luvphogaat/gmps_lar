<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CustomerInquiry extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $roomDetails;
    public $customerDetails;
    public function __construct($customerDetails, $roomDetails)
    {
        //
        $this->customerDetails = $customerDetails;
        $this->roomDetails = $roomDetails;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // dd($this->roomDetails);
        // return $this->view('view.name');
        return $this->from('no-reply@gangacottage.com', 'Ganga Cottage')
                ->subject('Reservation Inquiry - ' . $this->customerDetails['name'] . ' - ' . $this->roomDetails['tariff_name'])
                ->bcc('gangacottageranikhet@gmail.com')
                ->markdown('mails.customerInquiry');
    }
}
