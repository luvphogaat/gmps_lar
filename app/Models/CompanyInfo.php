<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CompanyInfo extends Model
{
    use HasFactory;

    protected $table = 'company_infos';

    protected $fillable = [
        'id', 'company_name', 'gst', 'cgst', 'sgst', 'address', 'phone', 'country', 'message', 'currency', 'state'
    ];
}
