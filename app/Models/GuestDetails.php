<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GuestDetails extends Model
{
    use HasFactory;
    protected $table = 'guest_details';


    protected $fillable = [
        'name', 'email', 'contact'
    ];

    public function reservation() {
        return $this->hasMany('App\Models\Reservation', 'guestId', 'id');
    }
}
