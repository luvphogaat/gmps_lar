<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inquiry extends Model
{
    use HasFactory;

    protected $table = 'inquiry';

    protected $fillable = [
        'id', 'name', 'email', 'phone', 'checkindate', 'checkoutdate', 'adult', 'children', 'room_id', 'noofrooms', 'message'
    ];
}
