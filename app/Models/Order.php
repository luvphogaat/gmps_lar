<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $table = "orders";
    protected $fillable = [
        'name', 'address','table_id', 'pay_status', 'order_type'
    ];

    public function orderdetail() {
        return $this->hasMany('App\Models\Order_Detail');
    }

    public function room() {
        return $this->belongsTo('App\Models\Rooms', 'table_id');
    }

}
