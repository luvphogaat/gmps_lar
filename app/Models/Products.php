<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    use HasFactory;

    protected $table = 'products';


    protected $fillable = ['id','name', 'price','image', 'active'];

    public function orderdetail() {
        return $this->hasMany('App\Models\Order_Detail');
    }

    public function cart() {
        return $this->hasMany('App\Models\Cart');
    }
}
