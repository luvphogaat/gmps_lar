<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    use HasFactory;
    protected $table = 'reservation';


    protected $fillable = [
        'reservation_number',
        'guestId',
        'roomId',
        'adultCount',
        'childCount',
        'checkin',
        'checkout',
        'nights',
        'advance_amount',
        'source',
        'total_amount',
        'sell_amount',
        'stay_status',
        'paid_amount'
    ];

    public function room() {
        return $this->hasMany('App\Models\Rooms', 'id', 'roomId');
    }

    public function guest() {
        return $this->belongsTo('App\Models\GuestDetails', 'guestId');
    }
}
