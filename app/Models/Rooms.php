<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rooms extends Model
{
    use HasFactory;

    protected $table = "rooms";
    protected $fillable = [
        'room_name', 'room_floor', 'room_type'
    ];

    public function reservation() {
        return $this->hasMany('App\Models\Reservation', 'roomId', 'id');
    }
}
