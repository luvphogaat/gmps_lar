<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tariff extends Model
{
    use HasFactory;
    protected $table = 'tariff';

    protected $fillable = [
        'id', 'tariff_name', 'price', 'cut_price', 'visibility', 'image'
    ];
}
