<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;
    protected $table = 'transactions';

    protected $fillable = [
        'id', 'paymentLink', 'amount', 'link_expiry', 'link_creation', 'currency', 'payment_id',
        'payment_link_id', 'payment_link_reference_id', 'payment_link_status', 'signature'
    ];
}
