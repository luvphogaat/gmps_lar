<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use App\Models\CompanyInfo;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Paginator::useBootstrap();
        // dd('hello', Auth::user());
        // if (Auth::check()) {
            // The user is logged in...
            // $id = Auth::user()->id;
            $companyInfo = CompanyInfo::where('id', '1')->first();
            view()->share('companyInfo', $companyInfo);
        // }

    }
}
