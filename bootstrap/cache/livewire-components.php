<?php return array (
  'orders.index' => 'App\\Http\\Livewire\\Orders\\Index',
  'orders.new-order' => 'App\\Http\\Livewire\\Orders\\NewOrder',
  'orders.view-single' => 'App\\Http\\Livewire\\Orders\\ViewSingle',
);