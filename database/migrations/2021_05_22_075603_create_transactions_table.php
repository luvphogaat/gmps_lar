<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            // $table->string('paymentLink');
            // $table->string('amount');
            // $table->string('link_expiry');
            // $table->string('link_creation');
            // $table->string('currency');
            // $table->string('payment_id')->defaut(NULL);
            // $table->string('payment_link_id')->defaut(NULL);
            // $table->string('payment_link_reference_id')->defaut(NULL);
            // $table->string('payment_link_status')->defaut(NULL);
            // $table->string('signature')->defaut(NULL);
            // $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            // $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->string('currency');
            $table->string('gateway_name')->defaut(NULL);
            $table->string('bank_name')->defaut(NULL);
            $table->string('payment_mode');
            $table->string('mid');
            $table->string('resp_code')->defaut(NULL);
            $table->string('resp_msg')->defaut(NULL);
            $table->text('txn_id')->defaut(NULL);
            $table->string('txn_amount')->defaut(NULL);
            $table->string('order_id')->defaut(NULL);
            $table->string('status')->defaut(NULL);
            $table->string('bank_txn_id')->defaut(NULL);
            $table->string('txn_date')->defaut(NULL);
            $table->text('checksum_hash')->defaut(NULL);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
