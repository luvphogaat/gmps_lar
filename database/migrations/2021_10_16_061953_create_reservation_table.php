<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservation', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('guestId');
            $table->bigInteger('roomId');
            $table->bigInteger('adultCount');
            $table->bigInteger('childCount');
            $table->date('checkin');
            $table->date('checkout');
            $table->bigInteger('nights');
            $table->string('source');
            $table->string('stay_status');
            $table->double('advance_amount', 20, 2);
            $table->double('paid_amount', 20, 2);
            $table->double('sell_amount', 20, 2);
            $table->double('total_amount', 20, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservation');
    }
}
