<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderPayDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_pay_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pay_mode');
            $table->double('total_amount_without_tax', 20, 2);
            $table->double('tax', 20, 2);
            $table->double('total_amount_with_tax', 20, 2);
            $table->double('discount', 20, 2);
            $table->double('received_amount', 20, 2);
            $table->double('changed_amount', 20, 2);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_pay_details');
    }
}
