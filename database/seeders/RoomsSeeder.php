<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class RoomsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('rooms')->insert([
            'room_name' => 'Cottage 1',
            'room_floor' => 'GF',
            'room_type' => 'Royal Cottage',
        ]);
        DB::table('rooms')->insert([
            'room_name' => 'Cottage 2',
            'room_floor' => 'GF',
            'room_type' => 'Super Deluxe Cottage',
        ]);
        DB::table('rooms')->insert([
            'room_name' => 'Cotttage 3',
            'room_floor' => '1F',
            'room_type' => 'Deluxe Cottage',
        ]);
        DB::table('rooms')->insert([
            'room_name' => 'Cottage 4',
            'room_floor' => 'GF',
            'room_type' => 'Deluxe Cottage',
        ]);
    }
}
