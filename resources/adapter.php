<?php
header('Access-Control-Allow-Methods: *');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Content-Type, Content-Language, Authorization');
header('Access-Control-Expose-Headers: Authorization');

# CONFIG
define('_DB_HOST', 'localhost');
define('_DB_NAME', 'cottage');
define('_DB_USER', 'root');
define('_DB_PASS', '');

# DB CONNECT
$connection = mysqli_connect(_DB_HOST, _DB_USER, _DB_PASS) or die ('Unable to connect to MySQL server.<br ><br >Please make sure your MySQL login details are correct.');
$db = mysqli_select_db($connection, _DB_NAME) or die ('request "Unable to select database."');

$unavailable = array();
	$sql = "SELECT DATE(Checkin) as checkin FROM booking_details WHERE DATE(Checkin)";
	$sql_result = mysqli_query ($connection, $sql) or die ('request "Could not execute SQL query" '.$sql);
	while ($row = mysqli_fetch_assoc($sql_result)) {
		$unavailable[] = $row['checkin'];
	}
echo(json_encode($unavailable));
exit();