<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Invoice Language Lines
    |--------------------------------------------------------------------------
    */

    'invoice'                => 'Invoice',
    'invoice_no'             => 'Invoice No.',
    'receipt'                => 'Receipt No.',
    'serial'                 => 'Serial No.',
    'date'                   => 'Invoice date',
    'seller'                 => 'Seller',
    'buyer'                  => 'Customer',
    'address'                => 'Address',
    'code'                   => 'Code',
    'vat'                    => 'VAT code',
    'phone'                  => 'Phone',
    'description'            => 'Particulars',
    'units'                  => 'Units',
    'quantity'               => 'Qty',
    'price'                  => 'Price',
    'discount'               => 'Discount',
    'tax'                    => 'Tax',
    'sub_total'              => 'Sub total',
    'total_discount'         => 'Total discount',
    'taxable_amount'         => 'Taxable amount',
    'total_taxes'            => 'Total taxes',
    'tax_rate'               => 'Tax rate',
    'total_amount'           => 'Total amount',
    'pay_until'              => 'Please pay until',
    'amount_in_words'        => 'Amount in words',
    'amount_in_words_format' => '%s %s and %s %s',
    'notes'                  => 'Notes',
    'shipping'               => 'Shipping',
    'paid'                   => 'Paid',
    'due'                    => 'Due',
    'gst_rate'               => 'Gst',
    'nights'                 => 'Nights'
];
