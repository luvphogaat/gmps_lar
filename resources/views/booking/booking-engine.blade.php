@extends('main')
@section('style-nonlogged')
<style>
html {
    background: url("{{ asset('images/carousal/1.jpeg')}}") no-repeat center center fixed;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
}
</style>
@endsection
@section('content')
<div class="container-fluid">
    <div class="container-fluid">
        {{-- Booking Engine --}}
        {{-- <form> --}}
            <div class="" style="position: relative;margin:0px auto;width:700px;height: 80vh; background: #fff; margin-right: 0px">
            <div class="container" style="height: 100%; width:85%">
                <div class="form-block" style="padding:30px 0px">
                    {{-- <form class="col s12" action="" method="">
                        <div class="row">
                            <div class="col s6">
                                <h6>Total Price</h6>
                            </div>
                            <div class="col s6">
                                <h6>Rs. 0.0</h6>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <i class="material-icons prefix">account_circle</i>
                                <input id="icon_prefix" type="text" name="name" required  value="{{ old('name') }}">
                                <label for="icon_prefix">Name</label>
                            </div>
                            <div class="input-field col s6">
                                <i class="material-icons prefix">mail</i>
                                <input id="icon_email" type="email" name="email" required  value="{{ old('email') }}">
                                <label for="icon_email">Email</label>
                            </div>
                            <div class="input-field col s6">
                                <i class="material-icons prefix">phone</i>
                                <input id="icon_telephone" type="tel" class="validate" name="phone" required  value="{{ old('phone') }}">
                                <label for="icon_telephone">Telephone</label>
                            </div>
                            <div class="input-field col s6">
                                <i class="material-icons prefix">person</i>
                                <input id="icon_guestCount" type="number" min="1" max="12" class="validate" name="phone" required  value="{{ old('guestNumber') }}">
                                <label for="icon_guestCount">Guest</label>
                            </div>
                            <div class="input-field col s6">
                                <i class="material-icons prefix">person</i>
                                <input id="icon_guestCount" type="number" min="1" max="12" class="validate" name="phone" required  value="{{ old('guestNumber') }}">
                                <label for="icon_guestCount">Check In Date</label>
                            </div>
                            <div class="input-field col s6">
                                <i class="material-icons prefix">person</i>
                                <input id="icon_guestCount" type="number" min="1" max="12" class="validate" name="phone" required  value="{{ old('guestNumber') }}">
                                <label for="icon_guestCount">Check Out Date</label>
                            </div>
                            <div class="input-field col s12 center" style="text-align:center">
                                {!! NoCaptcha::renderJs() !!}
                                {!! NoCaptcha::display() !!}
                            </div>
                        </div>
                        <div class="book-button">
                            <button class="btn btn-primary btn-round" style="width:100%">SEARCH</button>
                        </div>
                    </form> --}}
                    <form method="post" action="{{route('bookingEngine.user.details')}}">
                        @csrf
                        <div class="row">
                            <div class="input-field col s6">
                                <i class="material-icons prefix">account_circle</i>
                                <input id="icon_prefix" type="text" name="name" required  value="{{ old('name') }}">
                                <label for="icon_prefix">Name</label>
                            </div>
                            <div class="input-field col s6">
                                <i class="material-icons prefix">mail</i>
                                <input id="icon_email" type="email" name="email" required  value="{{ old('email') }}">
                                <label for="icon_email">Email</label>
                            </div>
                            <div class="input-field col s6">
                                <i class="material-icons prefix">phone</i>
                                <input id="icon_telephone" type="tel" class="validate" name="phone" required  value="{{ old('phone') }}">
                                <label for="icon_telephone">Telephone</label>
                            </div>
                            <div class="input-field col s6">
                                <i class="material-icons prefix">person</i>
                                <input id="icon_guestCount" type="number" min="1" max="12" class="validate" name="phone" required  value="{{ old('guestNumber') }}">
                                <label for="icon_guestCount">Guest</label>
                            </div>
                            <div class="input-field col s6">
                                <i class="material-icons prefix">person</i>
                                <input id="icon_guestCount" type="date" class="validate" name="phone" required  value="{{ old('guestNumber') }}" min="{{ date('Y-m-d') }}">
                                <label for="icon_guestCount">Check In Date</label>
                            </div>
                            <div class="input-field col s6">
                                <i class="material-icons prefix">person</i>
                                <input id="icon_guestCount" type="date" class="validate" name="phone" required  value="{{ old('guestNumber') }}">
                                <label for="icon_guestCount">Check Out Date</label>
                            </div>
                            <div class="input-field col s12 center" style="text-align:center">
                                {!! NoCaptcha::renderJs() !!}
                                {!! NoCaptcha::display() !!}
                            </div>
                        </div>
                    <div class="book-button">
                        <button class="btn btn-primary btn-round" style="width:100%">SEARCH</button>
                    </div>
                        {{-- <input type="text" name="amount" placeholder="RS 10" class="form-control"/>
                        <button type="submit" class="btn btn-primary mt2">Pay</button> --}}
                    </form>
                </div>
            </div>
            </div>
        {{-- </form> --}}
</div>
</div>

@endsection
@section('script-nonloggedin')
<script>
  $(document).ready(function(){
    $('.collapsible').collapsible();
  });
</script>
@endsection
