@extends('main')
@section('content')
<div class="row">
    <div class="col s12 m12 center" style="padding: 0px">
        <img src="{{asset('images/contact-us-1.jpg')}}" class="responsive-img" style="width:100%;height:350px;" />
        <br />
        <div class="container">
            <div class="row">
                <div class="col s12 m4">
                    <div class="card horizontal">
                        <div class="card-stacked">
                            <h3>Contact Us</h3>
                            @if ($errors->any())
                            <div class="alert alert-danger-contactus">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                            </div><br />
                            @endif
                            <div class="card-content" style="padding-bottom: 32px">
                                <form class="col s12" action="{{ route('contactus.save') }}" method="post">
                                    @csrf
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <i class="material-icons prefix">account_circle</i>
                                            <input id="icon_prefix" type="text" name="name" required  value="{{ old('name') }}">
                                            <label for="icon_prefix">Name</label>
                                        </div>
                                        <div class="input-field col s12">
                                            <i class="material-icons prefix">mail</i>
                                            <input id="icon_email" type="email" name="email" required  value="{{ old('email') }}">
                                            <label for="icon_email">Email</label>
                                        </div>
                                        <div class="input-field col s12">
                                            <i class="material-icons prefix">phone</i>
                                            <input id="icon_telephone" type="tel" class="validate" name="phone" required  value="{{ old('phone') }}">
                                            <label for="icon_telephone">Telephone</label>
                                        </div>
                                        <div class="input-field col s12">
                                            <i class="material-icons prefix">mode_edit</i>
                                            <textarea id="icon_prefix2" class="materialize-textarea" name="message"
                                                required></textarea>
                                            <label for="icon_prefix2">Message</label>
                                        </div>
                                        <div class="input-field col s12 center" style="text-align:center">
                                            {!! NoCaptcha::renderJs() !!}
                                            {!! NoCaptcha::display() !!}
                                        </div>
                                        <input type="hidden" name="recaptcha" id="recaptcha">
                                        <button class="btn-floating btn-large waves-effect waves-light center"
                                            type="submit" style=" background: rgb(109, 128, 79)"><i
                                                class="material-icons center">send</i></button>

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col s12 m8 center">
                    <div class="card horizontal">
                        <div class="card-stacked">
                            <div class="card-content">
                                <div class="row valign-wrapper">
                                    <div class="col s12 m6" style="font-size: 1.5rem;word-break: break-all">
                                        +91-8076502209,<br /> +91-7982255484,<br /> +91-9354237659</div>
                                    <div class="col s12 m6">
                                        <i class="material-icons prefix"
                                            style="font-size: 7rem; color: rgb(109, 128, 79)">phone</i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card horizontal">
                        <div class="card-stacked">
                            <div class="card-content">
                                <div class="row valign-wrapper">
                                    <div class="col s12 m6">
                                        <i class="material-icons prefix"
                                            style="font-size: 7rem;color: rgb(109, 128, 79)">email</i>
                                    </div>
                                    <div class="col s12 m6" style="font-size: 1.5rem;word-break: break-all">
                                        gangacottageranikhet@gmail.com</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card horizontal">
                        <div class="card-stacked">
                            <div class="card-content">
                                <div class="row valign-wrapper">
                                    <div class="col s12 m6" style="font-size: 1.5rem;word-break: break-all">
                                        Ganga Cottage, Pastora Par Near Binsar
                                        Mahadev Mandir, Ranikhet, Uttarakhand 263664
                                    </div>
                                    <div class="col s12 m6">
                                        <i class="material-icons prefix"
                                            style="font-size: 7rem;color: rgb(109, 128, 79)">place</i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col s12">
                <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3467.043719507754!2d79.34144601467213!3d29.66050568202358!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390a01a1546d8239%3A0x20c469e414e91ef6!2sGanga%20Cottage!5e0!3m2!1sen!2sin!4v1619097491087!5m2!1sen!2sin"
                    height="600" style="border:0;width:100%" allowfullscreen="" loading="lazy"></iframe>
            </div>
        </div>
    </div>
</div>
{{-- <script src="https://www.google.com/recaptcha/api.js?render={{ config('services.recaptcha.sitekey') }}"></script>
<script>
    grecaptcha.ready(function () {
        grecaptcha.execute('{{ config('
            services.recaptcha.sitekey ') }}', {
                action: 'contact'
            }).then(function (token) {
            if (token) {
                document.getElementById('recaptcha').value = token;
            }
        });
    });
</script> --}}
<script type="text/javascript">
    $('#reload').click(function () {
        $.ajax({
            type: 'GET',
            url: 'contactus/reload-captcha',
            success: function (data) {
                $(".captcha span").html(data.captcha);
            }
        });
    });

</script>
@endsection