@extends('main')
@section('content')
<style>
    .view {
        /*           border:double;*/
        margin-top: 30px;
    }

    @media only screen and (max-width: 768px) {
        .explore {
            padding-left: 0px !important;
        }

        .viewmatter {
            display: flex;
            flex-direction: column;
            text-align: center;


        }

        .view {
            float: left !important;
            /*               border:double;    */
            width: 100%;
            order: 1;
            margin-top: 10px;
        }

        .view1 {
            float: left !important;
            /*               border:double;    */
            width: 100%;
            order: 1;
        }

        .matter {
            float: right !important;
            /*               border:double;    */
            width: 100%;
            order: 2;
        }
    }
</style>
<div class="row">
    <div class="col s12 center">
        <h3><i class="mdi-content-send brown-text"></i></h3>
        <h4>Explore Ranikhet</h4>
        <br />
        <div class="container-fluid">
            <div class="row viewmatter" style="">
                <div class="col s12 m12 center p-1">
                    <p>
                        Ranikhet is a hill station and cantonment town in Almora district in the Indian state of
                        Uttarakhand. It is the home for the Military Hospital, Kumaon Regiment (KRC) and Naga Regiment
                        and is maintained by the Indian Army.
                        Ranikhet is at an altitude of 1,869 metres (6,132 ft) above sea level and within sight of the
                        western peaks of the Himalayas.
                    </p>
                </div>
                <div class="col m6 s12 matter">
                    <div>
                        <h3>History</h3>
                        <p style="text-align:justify">
                            Ranikhet, which means Queen's meadow in Hindi, gets its name from a local legend, which
                            states that it was here, that Raja Sudhardev won the heart of his queen, Rani Padmini, who
                            subsequently chose the area for her residence, giving it the name, Ranikhet, though no
                            palace exists in the area.
                            In 1869, the British established the headquarters of the Kumaon Regiment here and used the
                            station as a retreat from the heat of the Indian summer. At one time during British Raj, it
                            was also proposed as the summer headquarters of Government of India, in lieu of Shimla. In
                            1900, it had a summer population of 7,705, and its corresponding winter population was
                            measured in 1901 as 3,153
                            Ranikhet had been under Nepalese rule, and the Kumaonese (people of Kumaon Region) won it
                            under the leadership of their able General Kashi Nath Adhikari – after whom the small town
                            of Kashipur was named (which at one point of time was the gateway to the hills and is now an
                            educational and institutional hub) – with the help of Britishers at around 1816 and is a
                            part of India now.

                        </p>
                    </div>
                </div>
                <div class="col m6 s12 view1">
                    <video autoplay controls muted loop class="embed-responsive-item" style="width:100%;height:auto">
                        <source src="{{ asset('videos/nearbyplaces.mp4') }}" type=video/mp4> </video> </div> </div> <div
                            class="row viewmatter">
                        <div class="col s12 m6 view">
                            <img src="{{ asset('images/explore/Ashiyana_Park.jpg') }}" style="width:100%;height:auto"
                                class="responsive-img" />
                        </div>
                        <div class="col s12 m6 matter">
                            <h3>Ashiyana Park</h3>
                            <p style="text-align:justify">Ashiyana Park is situated in the midst of Ranikhet town. The
                                Park is specially designed & developed for children on jungle theme by Ranikhet Cantt.
                            </p>
                        </div>
                </div>
                <div class="row viewmatter">
                    <div class="col s12 m6 matter">
                        <h3>Mankameshwar Temple</h3>
                        <p style="text-align:justify">This temple is attached to the Nar Singh Maidan (Ground),
                            constructed by the Kumaon regiment. Opposite the temple is a Gurudwara and a Shawl factory.
                        </p>
                    </div>
                    <div class="col s12 m6 view">
                        <img src="{{ asset('images/explore/mankameshwar-temple.jpg') }}" style="width:100%;height:auto"
                            class="responsive-img" />
                    </div>
                </div>
                <div class="row viewmatter">
                    <div class="col s12 m6 view">
                        <img src="{{ asset('images/explore/Rani-Jheel.jpg') }}" style="width:100%;height:auto"
                            class="responsive-img" />
                    </div>
                    <div class="col s12 m6 matter">
                        <h3>Rani Jheel</h3>
                        <p style="text-align:justify">There is another jewel added to the beauty of town "Rani Jheel".
                            Located nearby Nar Singh Stadium, beneath the Veer Naari Awas it gives the visitors a chance
                            to relax and enjoy the ride of Boats.</p>
                    </div>
                </div>
                <div class="row viewmatter">
                    <div class="col s12 m6 matter">
                        <h3>Binsar Mahadev</h3>
                        <p style="text-align:justify">The artistic structure of the Binsar Mahadev Temple and its deity
                            Lord Shiva both adds positive vibes to our mind and soul. A beautiful stream flowing close
                            to the Binsar Mahadev Temple adds amazement
                            to the natural beauteousness of this sacred place. Binsar Mahadev Temple is situated amidst
                            beautiful pine and deodar trees. Apart from the temple an ashram, Binsar Mahadev also has
                            lovely cedar forest surroundings.
                        </p>
                    </div>
                    <div class="col s12 m6 view">
                        <img src="{{ asset('images/explore/binsar_mahadev.jpg') }}" style="width:100%;height:auto"
                            class="responsive-img" />
                    </div>
                </div>
                <div class="row viewmatter">
                    <div class="col s12 m6 view">
                        <img src="{{ asset('images/explore/Haidakhan_Babaji_Temple.jpg') }}"
                            style="width:100%;height:auto" class="responsive-img" />
                    </div>
                    <div class="col s12 m6 matter">
                        <h3>Haidakhan Temple</h3>
                        <p style="text-align:justify">Situated 4 km from Ranikhet, Haidakhan Temple (Hairakhan Temple or
                            Chilianaula) is dedicated to Lord Shiva made by Shri Haidakhan Maharaj who is said to be the
                            incarnation of Lord Shiva.
                            The splendid views of the gigantic Himalayan peaks are clearly visible from Haidakhan
                            Temple.
                        </p>
                    </div>
                </div>
                <div class="row viewmatter">
                    <div class="col s12 m6 matter">
                        <h3>Bhalu Dam</h3>
                        <p style="text-align:justify">It is an artificial small lake blessed with natural bounteousness,
                            panoramic views of the Himalayan Mountains and placid surroundings. The small but beautiful
                            garden nearby the dam is marvelous.
                            The dam site is open for all the tourists throughout the year. The views of the massive
                            Himalayan peaks are enchanting and ravishing from Bhalu Dam. It is an ideal spot for camping
                            and picnicking.St. Bridget Church It is an old church in Ranikhet town.
                        </p>
                    </div>
                    <div class="col s12 m6 view">
                        <img src="{{ asset('images/explore/bhalu-dam.jpg') }}" style="width:100%;height:auto"
                            class="responsive-img" />
                    </div>
                </div>
                <div class="row viewmatter">
                    <div class="col s12 m6 view">
                        <img src="{{ asset('images/explore/katarmal.jpg') }}" style="width:100%;height:auto"
                            class="responsive-img" />
                    </div>
                    <div class="col s12 m6 matter">
                        <h3>Katarmal (Sun Temple)</h3>
                        <p style="text-align:justify">It is the second most important temple to Sun God, the first one
                            being Sun Temple of Konark in Orissa. Katarmal is more than 800 years old. Situated 25 km
                            from Ranikhet this historical temple is an example of intricate sculpturing.</p>
                    </div>
                </div>
                <div class="row viewmatter">
                    <div class="col s12 m6 matter">
                        <h3>Jhula Devi temple</h3>
                        <p style="text-align:justify">Jhula Devi temple is situated at a distance of 7 kilometres (4 mi)
                            from the town of Ranikhet near Chaubatia. It is said that the dense jungle near the temple
                            was once full of wild animals, leopards and tigers used to attack local villagers. The
                            villagers prayed to Maa Durga for protection, and one day Goddess Durga came in a shephard's
                            dream and advised him to excavate her idol. This temple was constructed at the spot where
                            the shepherd found Goddess Durga's idol. Since then pilgrims come to make a wish to Goddess
                            Jhula Devi. And when their wish is fulfilled they come again to thank Jhula Devi Maa by
                            offering a bell to the temple. The popularity of the temple can be realized by number of
                            bells hanging over the temple walls. The temple priests have to timely shift the old bells
                            to a different place to make room for the new bells that are tied everyday. In addition to
                            bells, dried coconut and prasad is offered to the Goddess. This place has a calm environment
                            and a sense of spiritual peace to it.</p>
                    </div>
                    <div class="col s12 m6 view">
                        <img src="{{ asset('images/explore/jhula-devi-temple.jpg') }}" style="width:100%;height:auto"
                            class="responsive-img" />
                    </div>
                </div>
                <div class="row viewmatter">
                    <div class="col s12 m6 view">
                        <img src="{{ asset('images/explore/golf_course.jpg') }}" style="width:100%;height:auto"
                            class="responsive-img" />
                    </div>
                    <div class="col s12 m6 matter">
                        <h3>Golf Course</h3>
                        <p style="text-align:justify">Ranikhet Golf Course is one of the highest golf courses of Asia,
                            located 5 kilometres (3 mi) from main Ranikhet City. Ranikhet Golf Course is a 9-hole course
                            making it one of the prime attractions of Ranikhet. The green meadow of the golf course at
                            such high altitude is awe-inspiring. The golf club provides membership plans for outsiders
                            as well .Golf course is located in KALIKA.</p>
                    </div>
                </div>

            </div>
        </div>
    </div>

    @endsection