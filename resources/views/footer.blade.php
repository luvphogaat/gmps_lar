<footer class="page-footer" style="background-color:rgba(109, 128, 79, 0.6);text-align:center">
    <a href="https://www.instagram.com/ganga_cottage/" target="_blank">
        <img src="{{ asset('images/insta.png') }}" style="width:24px;height:24px;" />
    </a>
    <a href="https://www.facebook.com/Ganga-Cottage-840045896195938/?view_public_for=840045896195938" target="_blank">
        <img src="{{ asset('images/facebook.png') }}" style="width:24px;height:24px;" />
    </a>

    <!--          <div class="container">
                <div class="row">
                  <div class="col l6 s12">
                    <h5 class="white-text">Follow Us</h5>
                    <p class="grey-text text-lighten-4">
                        <img src="/public/images/facebook.png">
                        <img src="/public/images/twitter.png">
                        <img src="/public/images/google-plus.png">
                        <img src="/public/images/whatsapp.png">
                    </p>
                  </div>
                  <div class="col l4 offset-l2 s12">

                    <ul>
                      <li><a class="grey-text text-lighten-3" href="#!">Home</a></li>
                      <li><a class="grey-text text-lighten-3" href="#!">About Us</a></li>
                      <li><a class="grey-text text-lighten-3" href="#!">Gallery</a></li>
                      <li><a class="grey-text text-lighten-3" href="#!">Pricing</a></li>
                      <li><a class="grey-text text-lighten-3" href="#!">Booking</a></li>
                      <li><a class="grey-text text-lighten-3" href="#!">Contact Us</a></li>
                    </ul>
                  </div>
                </div>
              </div>

              -->
    <div class="footer-copyright">
        <div class="container center">
            © <?php echo date('Y') ?> Copyright Ganga Cottage
            <a class="grey-text text-lighten-4 right" href="#!"></a>
        </div>
    </div>
</footer>
<!--Start of Tawk.to Script-->

<script type="text/javascript" defer>
    var Tawk_API = Tawk_API || {},
        Tawk_LoadStart = new Date();
    (function () {
        var s1 = document.createElement("script"),
            s0 = document.getElementsByTagName("script")[0];
        s1.async = true;
        s1.src = 'https://embed.tawk.to/5aed6e4d227d3d7edc24fa6f/default';
        s1.charset = 'UTF-8';
        s1.setAttribute('crossorigin', '*');
        s0.parentNode.insertBefore(s1, s0);
    })();
</script>
<!--End of Tawk.to Script-->
