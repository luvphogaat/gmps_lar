@extends('main')
@section('content')
<style>
.gallery .col {
    padding: 10px;
}
.gallery .col img {
    height: 450px;
    background-color: grey;
}

@media screen and (max-width: 480px) {
    .gallery .col img {
    height: 100px;
} 
}

</style>
<div class="row">
    <div class="col s12 center">
        <h3><i class="mdi-content-send brown-text"></i></h3>
        <h4>Gallery</h4>
        <br />
        @if($images->count())
        <div class="row gallery">
            @foreach($images as $image)
                <div class="col m4 s6 center">
                    <img class="materialboxed" src="{{ asset('images/gallery/web'). '/'. $image->image }}" srcset="{{ asset('images/gallery/mobile'). '/'. $image->image }} 160w,{{ asset('images/gallery/web'). '/'. $image->image }} 590w" style="width:100%;">
                </div>
            @endforeach
            {{-- <div class="col s4 center">
                <img class="materialboxed" src="{{ asset('images/new/3.jpg') }}" style="width:100%; height: 500px;">
            </div>
            <div class="col s4 center">
                <img class="materialboxed" src="{{ asset('images/new/4.jpg') }}" style="width:100%; height: 500px;">
            </div>
            <div class="col s4 center">
                <img class="materialboxed" src="{{ asset('images/new/5.JPG') }}" style="width:100%; height: 500px;">
            </div>
            <div class="col s4 center">
                <img class="materialboxed" src="{{ asset('images/new/6.JPG') }}" style="width:100%; height: 500px;">
            </div>
            <div class="col s4 center">
                <img class="materialboxed" src="{{ asset('images/new/7.jpeg') }}" style="width:100%; height: 500px;">
            </div>
            <div class="col s4 center">
                <img class="materialboxed" src="{{ asset('images/new/8.jpg') }}" style="width:100%; height: 500px;">
            </div>
            <div class="col s4 center">
                <img class="materialboxed" src="{{ asset('images/new/9.jpg') }}" style="width:100%; height: 500px;">
            </div>
            <div class="col s4 center">
                <img class="materialboxed" src="{{ asset('images/new/11.jpg') }}" style="width:100%; height: 500px;">
            </div>
            <div class="col s4 center">
                <img class="materialboxed" src="{{ asset('images/new/12.jpg') }}" style="width:100%; height: 500px;">
            </div>
            <div class="col s4 center">
                <img class="materialboxed" src="{{ asset('images/new/13.jpg') }}" style="width:100%; height: 500px;">
            </div>
            <div class="col s4 center">
                <img class="materialboxed" src="{{ asset('images/new/14.jpg') }}" style="width:100%; height: 500px;">
            </div>
            <div class="col s4 center">
                <img class="materialboxed" src="{{ asset('images/new/15.jpg') }}" style="width:100%; height: 500px;">
            </div>
            <div class="col s4 center">
                <img class="materialboxed" src="{{ asset('images/new/16.jpg') }}" style="width:100%; height: 500px;">
            </div>
             <div class="col s4 center">
                <img class="materialboxed" src="{{ asset('images/new/19.jpg') }}" style="width:100%; height: 500px;">
            </div>
            <div class="col s4 center">
                <img class="materialboxed" src="{{ asset('images/new/17.jpg') }}" style="width:100%; height: 500px;">
            </div>
            <div class="col s4 center">
                <img class="materialboxed" src="{{ asset('images/new/18.jpg') }}" style="width:100%; height: 500px;">
            </div>
            <div class="col s4 center">
                <img class="materialboxed" src="{{ asset('images/new/20.jpg') }}" style="width:100%; height: 500px;">
            </div>
            <div class="col s4 center">
                <img class="materialboxed" src="{{ asset('images/new/21.jpeg') }}" style="width:100%; height: 500px;">
            </div>
            <div class="col s4 center">
                <img class="materialboxed" src="{{ asset('images/new/22.jpeg') }}" style="width:100%; height: 500px;">
            </div>
            <div class="col s4 center">
                <img class="materialboxed" src="{{ asset('images/new/23.jpeg') }}" style="width:100%; height: 500px;">
            </div>
            <div class="col s4 center">
                <img class="materialboxed" src="{{ asset('images/new/24.jpeg') }}" style="width:100%; height: 500px;">
            </div>
            <div class="col s4 center">
                <img class="materialboxed" src="{{ asset('images/new/25.jpeg') }}" style="width:100%; height: 500px;">
            </div>
            <div class="col s4 center">
                <img class="materialboxed" src="{{ asset('images/new/26.jpeg') }}" style="width:100%; height: 500px;">
            </div>
            <div class="col s4 center">
                <img class="materialboxed" src="{{ asset('images/new/27.jpg') }}" style="width:100%; height: 500px;">
            </div> --}}
        </div>
        @endif
    </div>
</div>
<script>
    $(document).ready(function(){
        $('.materialboxed').materialbox();
    });
</script>
@endsection