@extends('main')
@section('content')
<div id="index-banner" class="parallax-container">
    <div class="slider">
        <div class="fullwidth carousel carousel-slider">
            <a class="carousel-item" href="{{route('gallery')}}"><img src="{{ asset('images/carousal/1.jpeg')}}" srcset="{{ asset('images/carousal/1_mobile.jpeg')}} 360w"></a>
            <a class="carousel-item" href="{{route('gallery')}}"><img src="{{ asset('images/carousal/13.jpg')}}" srcset="{{ asset('images/carousal/13_mobile.jpg')}} 360w"></a>
            <a class="carousel-item" href="{{route('gallery')}}"><img src="{{ asset('images/carousal/18.jpg')}}" srcset="{{ asset('images/carousal/18_mobile.jpg')}} 360w"></a>
            <a class="carousel-item" href="{{route('gallery')}}"><img src="{{ asset('images/carousal/12.jpg')}}" srcset="{{ asset('images/carousal/12_mobile.jpg')}} 360w"></a>
            <a class="carousel-item" href="{{route('gallery')}}"><img src="{{ asset('images/carousal/26.jpeg')}}" srcset="{{ asset('images/carousal/26_mobile.jpeg')}} 360w"></a>
            <a class="carousel-item" href="{{route('gallery')}}"><img src="{{ asset('images/carousal/17.jpg')}}" srcset="{{ asset('images/carousal/17_mobile.jpg')}} 360w"></a>
        </div>
    </div>
</div>


<div class="container">
    <div class="section">
        <div class="row">
            <div class="col s12 center">
                <h3><i class="mdi-content-send brown-text"></i></h3>
                <h4>About Us</h4>
                <br />
                <p style="font-size: 20px;">
                    A Stay here is just so relaxing and peaceful to mind as its atmosphere and temperature makees you
                    feel so good which can only be felt , along with awesome view and surrouned by greenry. Best place
                    for all type of tourists
                </p>
                <p style="font-size: 20px;">
                    <b>Ranikhet - Meadows of the Queen</b>
                    The charming hill station of Ranikhet is located in the Almora district of Uttarakhand at an
                    elevation of 1,829 mts above sea level. Ranikhet has everything to pacify the nerves of an avid city
                    dweller.

                    The hill station Ranikhet, literally meaning - Queens Land, is an all season tourist-spot for the
                    admirers of the natural beauty. Ranikhet is a place that reflects the best of the great Himalayas.
                </p>
            </div>
        </div>
    </div>
</div>


<div class="parallax-container valign-wrapper">
    <div class="section no-pad-bot">
        <div class="container">
            <div class="row center">
                <h5 class="header col s12 light"></h5>
            </div>
        </div>
    </div>
    <div class="parallax"><img src="{{asset('images/new/3.jpg')}}" alt="Unsplashed background img 2" style="width:100%"></div>
</div>

<div class="container">
    <div class="section">

        <div class="row">
            <div class="col s12 center">
                <h3><i class="mdi-content-send brown-text"></i></h3>
                <h4>Features</h4>
                <br />
                <div class="row">
                    <div class="col s12 m4">
                        <div class="icon-block">
                            <h2 class="center brown-text">
                                <img src="{{asset('images/featuresImg/6_copy.jpg')}}" style="width:100%; height: 200px" />
                            </h2>
                            <h5 class="center">Pollution Free Environment</h5>
                        </div>
                    </div>

                    <div class="col s12 m4">
                        <div class="icon-block">
                            <h2 class="center brown-text">
                                <img src="{{asset('images/featuresImg/3_copy.jpg')}}" style="width:100%; height: 200px" />
                            </h2>
                            <h5 class="center">Himalayan View</h5>

                            {{-- <p class="light">By utilizing elements and principles of Material Design, we were able to create
                                a framework that incorporates components and animations that provide more feedback to users.
                                Additionally, a single underlying responsive system across all platforms allow for a more
                                unified user experience.</p> --}}
                        </div>
                    </div>

                    <div class="col s12 m4">
                        <div class="icon-block">
                            <h2 class="center brown-text">
                                <img src="{{asset('images/featuresImg/7.JPG')}}" style="width:100%;" />
                            </h2>
                            <h5 class="center">Big Spacious , Neat and Clean Room</h5>

                            {{-- <p class="light">We have provided detailed documentation as well as specific code examples to
                                help new users get started. We are also always open to feedback and can answer any questions
                                a user may have about Materialize.</p> --}}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 m4">
                        <div class="icon-block">
                            <h2 class="center brown-text">
                                <img src="{{asset('images/featuresImg/8.jpeg')}}" style="width:100%; height: 200px" />
                            </h2>
                            <h5 class="center">CCTV Security (24 x 7)</h5>
                        </div>
                    </div>

                    <div class="col s12 m4">
                        <div class="icon-block">
                            <h2 class="center brown-text">
                                <img src="{{asset('images/featuresImg/9.jpg')}}" style="width:100%; height: 200px" />
                            </h2>
                            <h5 class="center">Free Parking</h5>
                        </div>
                    </div>

                    <div class="col s12 m4">
                        <div class="icon-block">
                            <h2 class="center brown-text">
                                <img src="{{asset('images/featuresImg/2.JPG')}}" style="width:100%; height: 200px" />
                            </h2>
                            <h5 class="center">Garden View with Fountains</h5>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 m4">
                        <div class="icon-block">
                            <h2 class="center brown-text">
                                <img src="{{asset('images/featuresImg/5.jpg')}}" style="width:100%; height: 200px" />
                            </h2>
                            <h5 class="center">Bonfire</h5>
                        </div>
                    </div>

                    <div class="col s12 m4">
                        <div class="icon-block">
                            <h2 class="center brown-text">
                                <img src="{{asset('images/featuresImg/10.JPG')}}" style="width:100%; height: 200px" />
                            </h2>
                            <h5 class="center">Beautiful Environment</h5>
                        </div>
                    </div>

                    <div class="col s12 m4">
                        <div class="icon-block">
                            <h2 class="center brown-text">
                                <img src="{{asset('images/featuresImg/11.JPG')}}" style="width:100%; height: 200px" />
                            </h2>
                            <h5 class="center">Led TV + Satellite TV HD Channels</h5>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 right">
                        <a class="btn" href="{{route('gallery')}}" style="background: rgb(109,128,79)">View More</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


<div class="parallax-container valign-wrapper">
    <div class="section no-pad-bot">
        <div class="container">
            <div class="row center">
                <h5 class="header col s12 light"></h5>
            </div>
        </div>
    </div>
    <div class="parallax">
        <img src="{{asset('images/new/28.JPG')}}" alt="Unsplashed background img 3" />
    </div>
</div>
<div class="container">
    <div class="section">

        <div class="row">
            <div class="col s12 center">
                <h3><i class="mdi-content-send brown-text"></i></h3>
                <h4>Contact Us</h4>
                <br />
                {{-- <p class="left-align light">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam
                    scelerisque id nunc nec volutpat. Etiam pellentesque tristique arcu, non consequat magna
                    fermentum ac. Cras ut ultricies eros. Maecenas eros justo, ullamcorper a sapien id, viverra
                    ultrices eros. Morbi sem neque, posuere et pretium eget, bibendum sollicitudin lacus. Aliquam
                    eleifend sollicitudin diam, eu mattis nisl maximus sed. Nulla imperdiet semper molestie. Morbi
                    massa odio, condimentum sed ipsum ac, gravida ultrices erat. Nullam eget dignissim mauris, non
                    tristique erat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia
                    Curae;</p> --}}
                <div class="row">
                    <div class="col s12 m6 center">
                        {{-- <iframe src="https://www.google.com/maps/embed?pb=!1m26!1m12!1m3!1d892373.874714025!2d77.71310253794314!3d29.11053901215139!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m11!3e6!4m3!3m2!1d28.5631751!2d77.1936375!4m5!1s0x390a00e77413f44f%3A0xd1f4e6f9da63a7ec!2sGanga+Cottage%2C+Ranikhet%2C+Uttrakhand%2C+Talla+Dauda%2C+Uttarakhand+263664%2C+India!3m2!1d29.660261!2d79.3434525!5e0!3m2!1sen!2sin!4v1500895169232" frameborder="0" style="border:0;width:100%;min-height:400px" allowfullscreen></iframe> --}}
                        <iframe
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3467.043719507754!2d79.34144601510616!3d29.66050568202358!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390a01a1546d8239%3A0x20c469e414e91ef6!2sGanga%20Cottage!5e0!3m2!1sen!2sin!4v1618931099935!5m2!1sen!2sin"
                            style="border:0;width:100%;min-height:400px" allowfullscreen="" loading="lazy"></iframe>
                    </div>
                    <div class="col s12 m6 center">
                        @if ($errors->any())
                        <div class="alert alert-danger-contactus">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                        @endif
                        <form class="col s12" action="{{ route('contactus.save') }}" method="post">
                            @csrf
                            <div class="row">
                                <div class="input-field col s12">
                                    <i class="material-icons prefix">account_circle</i>
                                    <input id="icon_prefix" type="text" name="name" required  value="{{ old('name') }}">
                                    <label for="icon_prefix">Name</label>
                                </div>
                                <div class="input-field col s12">
                                    <i class="material-icons prefix">mail</i>
                                    <input id="icon_email" type="email" name="email" required  value="{{ old('email') }}">
                                    <label for="icon_email">Email</label>
                                </div>
                                <div class="input-field col s12">
                                    <i class="material-icons prefix">phone</i>
                                    <input id="icon_telephone" type="tel" class="validate" name="phone" required  value="{{ old('phone') }}">
                                    <label for="icon_telephone">Telephone</label>
                                </div>
                                <div class="input-field col s12">
                                    <i class="material-icons prefix">mode_edit</i>
                                    <textarea id="icon_prefix2" class="materialize-textarea" name="message"
                                        required>{{ old('product_name') }}</textarea>
                                    <label for="icon_prefix2">Message</label>
                                </div>
                                <div class="input-field col s12 center" style="text-align:center">
                                    {!! NoCaptcha::renderJs() !!}
                                    {!! NoCaptcha::display() !!}
                                </div>
                                <input type="hidden" name="recaptcha" id="recaptcha">
                                <button class="btn-floating btn-large waves-effect waves-light center"
                                    type="submit" style=" background: rgb(109, 128, 79)"><i
                                        class="material-icons center">send</i></button>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


{{-- <div class="parallax-container valign-wrapper">
    <div class="section no-pad-bot">
        <div class="container">
            <div class="row center">
                <h5 class="header col s12 light"></h5>
            </div>
        </div>
    </div>
    <div class="parallax">
        <img src="{{asset('images/new/14.jpg')}}" alt="Unsplashed background img 3" />
</div>
</div> --}}

<script>
    $(document).ready(function () {
        $('.fullwidth.carousel.carousel-slider').carousel({
            fullWidth: true,
            indicators: true,
            padding: 10,
            autoplay: true,
            time_constant: 100
        });
        autoplay();

        function autoplay() {
            $('.carousel').carousel('next');
            setTimeout(autoplay, 15000);
        }
        // $('.parallax').parallax();
    });
</script>
@endsection
