@extends('main')
@section('content')
<div class="container">
    <h3 class="center">Inquiry</h3>
    <br />
    <div class="row">
        <div class="col s12 m6 offset-m3 center">
            {{-- // Form Selection --}}
            <div class="row">
                <div class="col m12 s12 center">
                    @if ($errors->any())
                    <div class="alert alert-danger-contactus">
                        <ul style="padding:5px 10px">
                            @foreach ($errors->all() as $error)
                            <li style="text-left">{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />
                    @endif
                    <form class="col s12" action="{{ route('inquiry.save', $t_id) }}" method="post">
                        <input type="hidden" value="{{ $t_id }}" name="room_id" />
                        @csrf
                        <div class="input-field col s12">
                            <i class="material-icons prefix">account_circle</i>
                            <input id="icon_prefix" type="text" name="name" required value="{{ old('name') }}">
                            <label for="icon_prefix">Name</label>
                        </div>
                        <div class="input-field col s12">
                            <i class="material-icons prefix">mail</i>
                            <input id="icon_email" type="email" name="email" required value="{{ old('email') }}">
                            <label for="icon_email">Email</label>
                        </div>
                        <div class="input-field col s12">
                            <i class="material-icons prefix">phone</i>
                            <input id="icon_phone" type="tel" class="validate" name="phone" required
                                value="{{ old('phone') }}">
                            <label for="icon_phone">Telephone</label>
                        </div>
                        <div class="input-field col s12">
                            <i class="material-icons prefix">date_range</i>
                            <input id="icon_checkindate" type="text" class="validate datepicker" name="checkindate"
                                required value="{{ old('checkindate') }}">
                            <label for="icon_checkindate">Check-In Date</label>
                        </div>
                        <div class="input-field col s12">
                            <i class="material-icons prefix">date_range</i>
                            <input id="icon_checkoutdate" type="text" class="validate datepicker1" name="checkoutdate"
                                required value="{{ old('checkoutdate') }}">
                            <label for="icon_checkoutdate">Check-Out Date</label>
                        </div>
                        <div class="input-field col s12">
                            <i class="material-icons prefix">person</i>
                            <input id="icon_adult" type="number" class="validate" name="adult" required min="1" max="12"
                                value="{{ old('adult') }}">
                            <label for="icon_adult">Adult</label>
                        </div>
                        <div class="input-field col s12">
                            <i class="material-icons prefix">person</i>
                            <input id="icon_children" type="number" class="validate" name="children" required min="0"
                                max="15" value="{{ old('children') }}">
                            <label for="icon_children">Children</label>
                        </div>
                        <div class="input-field col s12">
                            <i class="material-icons prefix">person</i>
                            <input id="icon_children" type="number" class="validate" name="noofrooms" required min="1"
                                max="4" value="{{ old('noofrooms') }}">
                            <label for="icon_children">Number of Rooms</label>
                        </div>
                        <div class="input-field col s12">
                            <i class="material-icons prefix">email</i>
                            <textarea id="icon_prefix2" class="materialize-textarea" name="message"></textarea>
                            <label for="icon_prefix2">More Details</label>
                        </div>
                        <div class="input-field col s12 center" style="text-align:center">
                            {!! NoCaptcha::renderJs() !!}
                            {!! NoCaptcha::display() !!}
                        </div>
                        <input type="hidden" name="recaptcha" id="recaptcha">
                        <button class="btn btn-large waves-effect waves-light center" type="submit"
                            style=" background: rgba(109, 128, 79, 1)">
                            Submit
                        </button>
                    </form>
                </div>
            </div>
        </div>
        {{-- <div class="col s12 m6" style="overflow-y: scroll; max-height: 670px">
            @foreach ($tariffs as $tariff) 
            <div class="col s12 m12">
                <div class="card">
                    <div class="card-image">
                        <img src="{{ asset('images/' . $tariff->image)}}" style="height: 300px">
    </div>
    <div class="card-content">
        <div class="container-fluid">
            <span class="card-title" style="width:100%;">
                {{ $tariff->tariff_name }} (&#8377;
                {{ $tariff->price }}/- + taxes)
            </span>
        </div>
        <a class="waves-effect waves-light btn" href="{{ route('inquiry') }}">Select Now</a>
        <!--        <h6>For Booking Call 8076502209</h6>-->
    </div>
</div>
</div>
@endforeach
</div> --}}
</div>
</div>
<script>
    $(document).ready(function () {
        $('.datepicker').datepicker({
            defaultDate: new Date(),
            minDate: new Date(),
        });
        $('.datepicker1').datepicker({
            defaultDate: new Date(),
            minDate: new Date(),
        });
    });
</script>
@endsection