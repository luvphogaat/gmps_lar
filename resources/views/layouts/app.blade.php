<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/cottage_icon.ico') }}" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Caveat&family=Courgette&display=swap" rel="stylesheet">
        <title>Ganga Cottage | Dashboard</title>

        <!-- CSS  -->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
        <link href="{{ asset('css/materialize.css') }}" type="text/css" rel="stylesheet" media="screen,projection" />
        <link href="{{ asset('css/style.css') }}" type="text/css" rel="stylesheet" media="screen,projection" />
        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link href="{{ asset('css/material-dashboard.css?v=2.1.2') }}" rel="stylesheet" />
        @livewireStyles
    </head>
    <body class="font-sans antialiased">
        <div class="wrapper">
            @include('layouts.navigation')
            <div class="main-panel">
            @include('layouts.sidenav')
             <!-- Page Heading -->
            <!-- <header class="bg-white shadow">
                <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
                    {{ $header }}
                </div> -->
            <!-- </header> -->
            <div class="content">
                <main>
                    {{ $slot }}
                </main>
            </div>
            </div>
        </div>


        <!-- Scripts -->
        <script src="{{ asset('js/core/jquery.min.js') }}"></script>
        <script src="{{ asset('js/app.js') }}" defer></script>
        <script src="{{ asset('js/core/popper.min.js') }}"></script>
        <script src="{{ asset('js/core/bootstrap-material-design.min.js') }}"></script>
        <script src="{{ asset('js/plugins/perfect-scrollbar.jquery.min.js') }}"></script>
        <script src="{{ asset('js/material-dashboard.min.js?v=2.1.2') }}" type="text/javascript"></script>
        @yield('script')

        @livewireScripts
    </body>
</html>
