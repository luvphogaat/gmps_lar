<div class="sidebar" data-color="purple" data-background-color="white" data-image="{{ asset('images/sidebar-1.jpg') }}">
    <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
    <div class="logo"><a href="#" class="simple-text logo-normal">
            {{ Auth::user()->name }}
        </a></div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li class="nav-item {{ request()->is('dashboard') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('dashboard')}}">
                    <i class="material-icons">dashboard</i>
                    <p>Dashboard</p>
                </a>
            </li>
            @if (Auth::user()->hasRole('admin'))
            <li class="nav-item {{ request()->is('dashboard/company*') ? 'active' : '' }}">
                <a class="nav-link" data-toggle="collapse" href="#orderSideMenu" aria-expanded="false">
                    <i class="material-icons">content_paste</i>
                    <p>Orders<b class="caret"></b></p>
                </a>
                <div class="collapse" id="orderSideMenu" style="">
                    <ul class="nav">
                      <li class="nav-item ">
                        <a class="nav-link" href="{{ route('order.create')}}">
                          <span class="sidebar-mini"> CNO </span>
                          <span class="sidebar-normal"> Create New Order </span>
                        </a>
                      </li>
                      <li class="nav-item ">
                        <a class="nav-link" href="{{ route('order.index')}}">
                          <span class="sidebar-mini"> VO </span>
                          <span class="sidebar-normal"> View Orders </span>
                        </a>
                      </li>
                    </ul>
                  </div>
            </li>
            <li class="nav-item {{ request()->is('dashboard/company*') ? 'active' : '' }}">
                <a class="nav-link" data-toggle="collapse" href="#reservationSideMenu" aria-expanded="false">
                    <i class="material-icons">content_paste</i>
                    <p>Reservations<b class="caret"></b></p>
                </a>
                <div class="collapse" id="reservationSideMenu" style="">
                    <ul class="nav">
                      <li class="nav-item ">
                        <a class="nav-link" href="{{ route('dashboard.reservation.new') }}">
                          <span class="sidebar-mini"> CNR </span>
                          <span class="sidebar-normal"> Create New Reservation </span>
                        </a>
                      </li>
                      <li class="nav-item ">
                        <a class="nav-link" href="{{ route('dashboard.reservation.list') }}">
                          <span class="sidebar-mini"> VR </span>
                          <span class="sidebar-normal"> View Reservations </span>
                        </a>
                      </li>
                    </ul>
                  </div>
            </li>
            <li class="nav-item {{ request()->is('dashboard/transaction*') ? 'active' : '' }}">
                <a class="nav-link" data-toggle="collapse" href="#paymentSideMenu" aria-expanded="false">
                    <i class="material-icons">content_paste</i>
                    <p>Payments<b class="caret"></b></p>
                </a>
                <div class="collapse" id="paymentSideMenu" style="">
                    <ul class="nav">
                      <li class="nav-item {{ request()->is('dashboard/transaction') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('dashboard.transaction.link.create') }}">
                          <span class="sidebar-mini"> CNPL </span>
                          <span class="sidebar-normal"> Create New Payment Link </span>
                        </a>
                      </li>
                      <li class="nav-item {{ request()->is('dashboard/transaction/create') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('dashboard.transaction.index') }}">
                          <span class="sidebar-mini"> VP </span>
                          <span class="sidebar-normal"> View Payments </span>
                        </a>
                      </li>
                    </ul>
                  </div>
            </li>
            <li class="nav-item {{ request()->is('dashboard/tariff*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('tariff.index') }}">
                    <i class="material-icons">attach_money</i>
                    <p>Room Tariff</p>
                </a>
            </li>
            <li class="nav-item {{ request()->is('dashboard/gallery*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('dashboard.gallery.view') }}">
                    <i class="material-icons">perm_media</i>
                    <p>Gallery</p>
                </a>
            </li>
            <li class="nav-item {{ request()->is('dashboard/company*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('company-info.index') }}">
                    <i class="material-icons">info</i>
                    <p>Company Info</p>
                </a>
            </li>
            @endif
            <li class="nav-item">
            <form method="POST" action="{{ route('logout') }}">
            @csrf
                <a class="nav-link" href="#">
                    <i class="material-icons">power_settings_new</i>
                            <p>Logout</p>
                    </form>
                </a>
                </form>
            </li>
        </ul>
    </div>
</div>
