<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header purple text-white">
                    <h3>Current Orders</h3>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table tableed" cellspacing="20" cellpadding="20">
                            <thead class=" text-primary">
                                <th>Sno</th>
                                <th width="40%">
                                    Room Name
                                </th>
                                <th>
                                    Total Amount
                                </th>
                                <th>
                                    Status
                                </th>
                                <!-- <th>
                                    Dis (%)
                                </th> -->
                                <th colspan="6">
                                    #
                                </th>
                            </thead>
                            <tbody class="addMoreProduct">
                                @foreach($orders as $key => $order)
                                {{-- {{ dd($order) }} --}}
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td width="40%">
                                        <div class="form-outline">
                                            {{ $order->room->room_name }}
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-outline">
                                            {{-- <input type="number" name="quantity[]" id="quantity"
                                                class="form-control quantity custom-text-css"
                                                value="{{ $cart->product_qty }}" min="0" /> --}}
                                                {{ $this->currencyFormatter($order->orderdetail->sum('amount')  ) }}
                                        </div>
                                    </td>
                                    <td width="15%">
                                        <div class="form-outline" style="text-transform: uppercase">
                                            {{-- <input type="number" name="price[]" id="price"
                                                value="{{ $cart->product_price }}"
                                                class="form-control price custom-text-css" /> --}}
                                                {{ $order->pay_status }}
                                        </div>
                                    </td>
                                    <td class="text-center" width="10%">
                                        <!-- <a href="#" class="btn btn-danger delete">
                                                <i class="material-icons" wire:click="removeProduct({{ $order->id }})">close</i>
                                            </a> -->
                                        {{-- <button type="button" rel="tooltip" class="btn btn-danger btn-sm">
                                            <i class="material-icons"
                                                wire:click="removeProduct({{ $order->id }})">close</i>
                                        </button> --}}
                                        <a href="{{ route('order.show', $order->id) }}" class="btn btn-success">
                                            <i class="material-icons">add</i>Add More
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header purple text-white">
                    <h3>Completed Orders</h3>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table tableed" cellspacing="20" cellpadding="20">
                            <thead class=" text-primary">
                                <th>Sno</th>
                                <th width="40%">
                                    Room Name
                                </th>
                                <th>
                                    Total Amount
                                </th>
                                <th>
                                    Status
                                </th>
                                <!-- <th>
                                    Dis (%)
                                </th> -->
                                <th colspan="6">
                                    #
                                </th>
                            </thead>
                            <tbody class="addMoreProduct">
                                @foreach($ordersCom as $key => $order)
                                {{-- {{ dd($order) }} --}}
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td width="40%">
                                        <div class="form-outline">
                                            {{ $order->room->room_name }} - {{ $order->name }}
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-outline">
                                            {{-- <input type="number" name="quantity[]" id="quantity"
                                                class="form-control quantity custom-text-css"
                                                value="{{ $cart->product_qty }}" min="0" /> --}}
                                                {{ $this->currencyFormatter($order->orderdetail->sum('amount')  ) }}
                                        </div>
                                    </td>
                                    <td width="15%">
                                        <div class="form-outline" style="text-transform: uppercase">
                                            {{-- <input type="number" name="price[]" id="price"
                                                value="{{ $cart->product_price }}"
                                                class="form-control price custom-text-css" /> --}}
                                                {{ $order->pay_status }}
                                        </div>
                                    </td>
                                    <td class="text-center" width="10%">
                                        <!-- <a href="#" class="btn btn-danger delete">
                                                <i class="material-icons" wire:click="removeProduct({{ $order->id }})">close</i>
                                            </a> -->
                                        {{-- <button type="button" rel="tooltip" class="btn btn-danger btn-sm">
                                            <i class="material-icons"
                                                wire:click="removeProduct({{ $order->id }})">close</i>
                                        </button> --}}
                                        <a href="{{ route('order.show', $order->id) }}" class="btn btn-success">
                                            View
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
