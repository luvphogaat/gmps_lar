<div class="container-fluid">
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header purple text-white">
                    <h3>Order #{{ $order_number }}</h3>
                </div>
                <div class="my-2">
                <!-- <form > -->
                        <div class="container-fluid">
                                <select name="product[]" id="" class="form-control custom-select-css" wire:model="product_code" wire:change="InserttoCart" data-live-search="true">
                                    <option value="" selected>Select Item</option>
                                    @foreach( $products as $product )
                                    <option value="{{ $product->id }}" wire:key="{{  $product->id }}"
                                        data-price="{{$product->price}}">
                                        {{ $product->name }}</option>
                                    @endforeach
                                </select>
                        </div>
                </div>
                <form action="{{ route('order.store') }}" method="post">
                    @csrf
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table tableed" cellspacing="20" cellpadding="20">
                                <thead class=" text-primary">
                                    <th></th>
                                    <th width="40%">
                                        Product Name
                                    </th>
                                    <th>
                                        Qty
                                    </th>
                                    <th>
                                        Price
                                    </th>
                                    <!-- <th>
                                    Dis (%)
                                </th> -->
                                    <th colspan="6">
                                        Total
                                    </th>
                                    <!-- <th>
                                        <a href="#" class="btn btn-success add_more">
                                            <i class="material-icons">add</i>
                                        </a>
                                    </th> -->
                                </thead>
                                <tbody class="addMoreProduct">
                                @foreach($productInCart as $key => $cart)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td  width="40%">
                                            <div class="form-outline">
                                                <input type="hidden" name="product_id[]" value="{{ $cart->product->id }}"/>
                                                <input type="text" name="product[]" value="{{ $cart->product->name }}" readonly class="custom-text-css" />
                                            </div>
                                        </td>
                                        <td >
                                            <div class="form-outline">
                                                <input
                                                    type="number"
                                                    name="quantity[]"
                                                    id="quantity"
                                                    class="form-control quantity custom-text-css"
                                                    value="{{ $cart->product_qty }}" min="0"
                                                    :key="{{ $cart->id }}"
                                                    wire:change="ChangeQty({{ $cart->id }}, $event.target.value)"
                                                />

                                            </div>
                                        </td>
                                        <td width="15%">
                                            <div class="form-outline">
                                                <input
                                                    type="number"
                                                    name="price[]"
                                                    id="price"
                                                    value="{{ $cart->product_price }}"
                                                    class="form-control price custom-text-css"
                                                />
                                            </div>
                                        </td>
                                        <td width="15%">
                                            <div class="form-outline">
                                                <input type="number" name="total_amount[]" id="total_amount" value="{{ $cart->product_qty * $cart->product_price }}"
                                                    class="form-control total_amount custom-text-css" />
                                            </div>
                                        </td>
                                        <td class="text-center" width="10%">
                                            <!-- <a href="#" class="btn btn-danger delete">
                                                <i class="material-icons" wire:click="removeProduct({{ $cart->id }})">close</i>
                                            </a> -->
                                            <button type="button" rel="tooltip" class="btn btn-danger btn-sm">
                                                <i class="material-icons" wire:click="removeProduct({{ $cart->id }})">close</i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header purple text-white">
                    <h3>Total: <b class="total">{{ $totalPrice ?? '0.00' }}</b></h3>
                </div>
                <div class="card-body">
                    <div class="conatiner-fluid">
                        <div class="form-group">
                            <label class="" for="table_id">Select Room/Table</label>
                            <select name="table_id" id="table_id" class="form-control table_id "
                                data-style="btn btn-link" required>
                                <option value="" selected></option>
                                @foreach( $rooms as $room )
                                <option value="{{ $room->id }}">{{ $room->room_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="paystatus_id">Payment Status</label>
                            <select class="form-control paystatus_id" data-style="btn btn-link" id="paystatus_id" wire:model="pay_type"
                                required>
                                <option value="pending" selected>Unpaid</option>
                                <option value="paid">Paid</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group bmd-form-group" style="width: 100%">
                        <button type="submit" class="btn btn-primary" style="width: 100%">SAVE<div
                                class="ripple-container"></div>
                        </button>
                        {{-- <button type="button" class="btn btn-info" style="width: 100%" onclick="PrintReceiptContent('print')">
                            Print<div class="ripple-container"></div>
                        </button> --}}
                    </div>
                    <!-- </form> -->
                </div>
            </div>
        </div>
    </div>
</div>
