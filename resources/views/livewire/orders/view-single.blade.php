<div class="container-fluid">
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header purple text-white">
                    <h3>Order</h3>
                </div>
                <div class="my-2">
                <!-- <form > -->
                        <div class="container-fluid">
                                <select name="product[]" id="" class="form-control custom-select-css" wire:model="product_code" wire:change="InserttoOrder">
                                    <option value="" selected>Select Item</option>
                                    @foreach( $products as $product )
                                    <option value="{{ $product->id }}" wire:key="{{  $product->id }}"
                                        data-price="{{$product->price}}">
                                        {{ $product->name }}</option>
                                    @endforeach
                                </select>
                        </div>
                </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table tableed" cellspacing="20" cellpadding="20">
                                <thead class=" text-primary">
                                    <th></th>
                                    <th width="40%">
                                        Product Name
                                    </th>
                                    <th>
                                        Qty
                                    </th>
                                    <th>
                                        Price
                                    </th>
                                    <!-- <th>
                                    Dis (%)
                                </th> -->
                                    <th colspan="6">
                                        Total
                                    </th>
                                    <!-- <th>
                                        <a href="#" class="btn btn-success add_more">
                                            <i class="material-icons">add</i>
                                        </a>
                                    </th> -->
                                </thead>
                                <tbody class="addMoreProduct">
                                @foreach($order_details->orderdetail as $key => $order)
                                {{-- {{ $order }} --}}
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td  width="40%">
                                            <div class="form-outline">
                                                <input type="hidden" name="product_id[]" value="{{ $order->product->id }}"/>
                                                <input type="text" name="product[]" value="{{ $order->product->name }}" readonly class="custom-text-css" />
                                            </div>
                                        </td>
                                        <td >
                                            <div class="form-outline">
                                                <input
                                                    type="number"
                                                    name="quantity[]"
                                                    id="quantity"
                                                    class="form-control quantity custom-text-css"
                                                    value="{{ $order->quantity }}"
                                                    min="0"
                                                    {{-- wire:model="qty_value.{{ $order->id }}" --}}
                                                    :key="{{ $order->id }}"
                                                    wire:change="ChangeQty({{ $order->id }}, $event.target.value)"
                                                />

                                            </div>
                                        </td>
                                        <td width="15%">
                                            <div class="form-outline">
                                                <input type="number" name="price[]" id="price" value="{{ $order->unitprice }}"
                                                    class="form-control price custom-text-css" readonly />
                                            </div>
                                        </td>
                                        <td width="15%">
                                            <div class="form-outline">
                                                <input type="number" name="total_amount[]" id="total_amount" value="{{ $order->quantity * $order->unitprice }}"
                                                    class="form-control total_amount custom-text-css" readonly />
                                            </div>
                                        </td>
                                        <td class="text-center" width="10%">
                                            <!-- <a href="#" class="btn btn-danger delete">
                                                <i class="material-icons" wire:click="removeProduct({{ $order->id }})">close</i>
                                            </a> -->
                                            <button type="button" rel="tooltip" class="btn btn-danger btn-sm">
                                                <i class="material-icons" wire:click="removeProduct({{ $order->id }})">close</i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
            </div>
        </div>
        <div class="col-md-4">
            <form action="{{ route('order.update', $order_details->id) }}" method="post">
            @csrf
            @method('PUT')
            {{-- {{ dd('hello')}} --}}
            <div class="card">
                <div class="card-header purple text-white">
                    <h3>Total: <b class="total">{{ $this->currencyFormatter($order_details->orderdetail->sum('amount')) }}</b></h3>
                </div>
                <div class="card-body">
                    <div class="conatiner-fluid">
                        <div class="form-group">
                            <label class="" for="table_id">Select Room</label>
                            {{-- <select name="table_id" id="table_id" class="form-control table_id "
                                data-style="btn btn-link" required>
                                @foreach( $rooms as $room )
                                <option value="{{ $room->id }}" {{ $room->id == $order_details->table_id  ? 'selected' : '' }}>{{ $room->room_name }}</option>
                                @endforeach
                            </select> --}}
                            {{-- <input type="text" readonly value="{{ $ro///om->room_name }}" /> --}}
                        </div>
                        {{-- {{ dd ($order_details)}} --}}
                        <div class="form-group">
                            <label for="paystatus_id">Payment Status</label>
                            @if ($order_details->pay_status == 'paid')
                                <input type="text" readonly value="{{ $order_details->pay_status }}" />
                            @else
                                <select name="pay_status" class="form-control paystatus_id" data-style="btn btn-link" id="paystatus_id" wire:model="pay_type"
                                    required>
                                    <option value="pending">Unpaid</option>
                                    <option value="paid">Paid</option>
                                </select>
                            @endif
                        </div>
                        <div class="form-group" x-show="pay_type == 'paid'">
                            <label for="paystatus_id">Payment Type</label>
                            <select class="form-control paystatus_id" data-style="btn btn-link" id="paystatus_id"
                                required>
                                <option value="Cash">Cash</option>
                                <option value="Upi-GooglePay">Google Pay</option>
                                <option value="Upi-Paytm">Paytm</option>
                                <option value="Upi-Paytm-Wallet">Paytm Wallet</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group bmd-form-group" style="width: 100%">
                        @if ($order_details->pay_status == 'paid')
                            <a href="{{ route('dashboard.receipt.food.show', $order_details->id) }}" type="button" class="btn btn-primary" target="__blank" style="width: 100%">PRINT BILL<div
                                class="ripple-container"></div>
                            </a>
                        @else
                            <button type="submit" class="btn btn-primary" style="width: 100%">SAVE<div
                                class="ripple-container"></div>
                            </button>
                        @endif
                        {{-- <button type="button" class="btn btn-info" style="width: 100%" onclick="PrintReceiptContent('print')">
                            Print<div class="ripple-container"></div>
                        </button> --}}
                    </div>
                    <!-- </form> -->
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
