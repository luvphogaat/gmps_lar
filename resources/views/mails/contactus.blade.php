@component('mail::message')

Thanks for reaching out to us. 

We will get back to you shortly, In the meanwhile please go through our website to find relevant details..

If Urgent please reach us at   +91-8076502209, +91-7982255484, +91-9354237659


Name: {{ $customerDetails['name'] }} <br>
Email: {{ $customerDetails['email'] }} <br>
Phone: {{ $customerDetails['phone'] }} <br>
Message: {{ $customerDetails['message'] }} <br>


Thanks & Regards,

Ganga Cottage

@endcomponent