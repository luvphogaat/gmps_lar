@component('mail::message')

# Inquiry Details

Thanks for reaching out to us. 

We will get back to you shortly, In the meanwhile please go through our website to find relevant details..

If Urgent please reach us at   +91-8076502209, +91-7982255484, +91-9354237659


Name: {{ $customerDetails['name'] }} <br>
Email: {{ $customerDetails['email'] }} <br>
Phone: {{ $customerDetails['phone'] }} <br>
Adults: {{ $customerDetails['adult'] }} <br>
Children: {{ $customerDetails['children'] }} <br>
Check-In Date: {{ $customerDetails['checkindate'] }} <br>
Check-Out Date: {{ $customerDetails['checkoutdate'] }} <br>
No of Rooms: {{ $customerDetails['noofrooms'] }} <br>
More Details: {{ $customerDetails['message'] }} <br>
Room Name: {{ $roomDetails['tariff_name'] }}



Thanks & Regards,

Ganga Cottage

@endcomponent