<!DOCTYPE html>
<html lang="en">

<head>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-119070219-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-119070219-1');
        </script>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/cottage_icon.ico') }}" />
    <meta name="facebook-domain-verification" content="b8r2y04ixbuoznnuyst8bvc3ppcwq3" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="https://fonts.gstatic.com">
    <link rel="preconnect" href="https://fonts.googleapis.com/css2?family=Caveat&family=Courgette&display=swap" rel="stylesheet">
    <title>Ganga Cottage</title>

    <!-- CSS  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="{{ asset('css/materialize.css') }}" type="text/css" rel="stylesheet" media="screen,projection" />
    <link href="{{ asset('css/style.css') }}" type="text/css" rel="stylesheet" media="screen,projection" />
    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.lazyload/1.9.1/jquery.lazyload.min.js" integrity="sha512-jNDtFf7qgU0eH/+Z42FG4fw3w7DM/9zbgNPe3wfJlCylVDTT3IgKW5r92Vy9IHa6U50vyMz5gRByIu4YIXFtaQ==" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function(){
            $('img').lazyload();
        });
    </script>
    @yield('style-nonlogged')

</head>

<body>
    <div class="navbar-fixed">
        <nav class="white" role="navigation">
            <div class="nav-wrapper container">
                <a href="{{ route('index') }}" class="brand-logo" style="padding-left:10px;"><img
                        src="{{ asset('images/logo_new/GC_New_1.png') }}" width="100px" height="60px"></a>
                <ul class="right hide-on-med-and-down">
                    <li  class="{{ request()->is('/') ? 'active' : '' }}"><a href="{{ route('index')}}">Home</a></li>
                    {{-- <li><a href="{{ route('index')}}">About Us</a></li> --}}
                    <li class="{{ request()->is('explore') ? 'active' : '' }}"><a href="{{ route('explore')}}" >Explore Ranikhet</a></li>
                    <li class="{{ request()->is('gallery') ? 'active' : '' }}"><a href="{{ route('gallery')}}" >Gallery</a></li>
                    <li class="{{ request()->is('tariff') ? 'active' : '' }}"><a href="{{ route('tariff')}}">Tariff</a></li>
                    {{-- <li><a href="{{ route('index')}}">Booking</a></li> --}}
                    <li  class="{{ request()->is('contactus') ? 'active' : '' }}"><a href="{{ route('contactus')}}" >Contact Us</a></li>
                    <li class="{{ request()->is('login') ? 'active' : '' }}"><a href="{{ route('login')}}" >Staff Login</a></li>
                </ul>
                <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
            </div>
        </nav>
    </div>
    <ul id="nav-mobile" class="sidenav fixed">
        <li><a href="{{ route('index')}}">Home</a></li>
        {{-- <li><a href="{{ route('index')}}">About Us</a/></li> --}}
        <li><a href="{{ route('explore')}}">Explore Ranikhet</a></li>
        <li><a href="{{ route('gallery')}}">Gallery</a></li>
        <li><a href="{{ route('index')}}">Tariff</a></li>
        {{-- <li><a href="{{ route('index')}}">Booking</a></li> --}}
        <li><a href="{{ route('contactus')}}">Contact Us</a></li>
        <li><a href="{{ route('login')}}" >Staff Login</a></li>
    </ul>
    <main class="container-fluid" style="min-height: 80vh;">
        @yield('content')
    </main>
    @extends('footer')
    <!--  Scripts-->

    <script src="{{ asset('js/materialize.js') }}"></script>
    <script src="{{ asset('js/init.js') }}"></script>
    @yield('script-nonloggedin')

</body>

</html>
