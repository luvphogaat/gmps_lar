<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Gallery') }}
        </h2>
    </x-slot>
    <style>
        .gallery .col {
            padding: 10px;
        }

        .gallery .col img {
            height: 450px;
            background-color: grey;
        }

        @media screen and (max-width: 480px) {
            .gallery .col img {
                height: 100px;
            }
        }

    </style>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                <a class="btn btn-success waves-effect waves-light" href="{{ route('dashboard.gallery.create') }}" role="button">Add New Image</a>
                    <div class="row">
                        <div class="col col-s-12 center">
                            <br />
                            @if($images->count())
                            <div class="row gallery">
                                @foreach($images as $image)
                                <div class="col col-md-6 col-s-12 center">
                                    <img class="materialboxed"
                                        src="{{ asset('images/gallery/web'). '/'. $image->image }}"
                                        srcset="{{ asset('images/gallery/mobile'). '/'. $image->image }} 160w,{{ asset('images/gallery/web'). '/'. $image->image }} 590w"
                                        style="width:100%;">
                                </div>
                                @endforeach
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('.materialboxed').materialbox();
        });

    </script>
</x-app-layout>
