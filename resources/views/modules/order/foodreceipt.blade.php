<!DOCTYPE html>
<html lang="en">

<head>
    <title>Invoice</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <style type="text/css" media="screen">
        @media print {
            @page {
                margin: 0 auto;
                padding:0px;
                size: a8 portrait;
            }

            html {
                direction: rtl;
            }

            body {
                page-break-inside: avoid;
            }

            html,
            body {
                margin: 0px;
                padding: 0px;

            }
            .body {
                padding: 0px;
                margin: 0px;
            }

            #printContainer {
                width: 100%;
                margin: 0px;
                text-align: justify;
            }

            .text-center {
                text-align: center;
            }
            .list-header {
                width: 100% !important;
                th, td {
                    font-family: 'arial';
                    font-size: 15px;
                }
            }
            .list-header tr {
                border-bottom: 1px solid black;
                border-collapse: collapse;
            }

            .table-items td {
                border-top: 1px solid #dee2e6;
            }

            /* .list-header          t {
                border-top: 1px solid #000;
            } */
            .invoice-header {
                td {
                    font-family: 'DejaVu Sans, sans-serif';
                    font-size:15px;
                }
            }
            .text-center {
                text-align: center;
            }
            .text-right {
                text-align: right !important;
            }
        }

    </style>
</head>

<body class="body" style="text-align: center; padding: 0px; margin: -60px !important; padding-top: -60px; font-family: 'DejaVu Sans, sans-serif'; font-weight:300px">
    <header>
        <h1 class="text-center" style="margin: 0px; margin-bottom: 5px;padding: 0px; font-size: 60px">
            {{-- <img src="{{ asset('images/logo_new/GC_New_1.png') }}" /> --}}
            {{-- <img src="data:image/svg+xml;base64,{{base64_encode($orderReceipt[1])}}"/> --}}
            GC
        </h1>
        <h2 id="slogan" style="margin-top:0; font-size: 20px !important" class="text-center">GANGA COTTAGE</h2>
        <p style="font-size: 15px; margin-bottom: 0px; margin:0px auto; width: 80%">Pastora Par Near Binsar Mahadev Mandir, Ranikhet, Uttarakhand-263664</p>
        <p style="font-size: 15px; margin: 0px;">GST NO : 05ESDPP1862E1ZU</p>
        <p style="font-size: 15px; margin: 0px">FSSAI NO : </p>
        <p style="font-size: 17px; margin: 0px; font-weight: bolder">TAX INVOICE</p>
    </header>
    <hr>
    <div id='printContainer' style="text-align:center">
        <table class="invoice-header">
            <tr>
                <td>Invoice Num:</td>
                <td>GC/{{date("dmy", strtotime($orderReceipt[0]->created_at))}}/{{$orderReceipt[0]->id }}</td>
            </tr>
            <tr>
                <td>Invoice Date:</td>
                <td>{{ date("d/m/Y H:i:s") }}</td>
            </tr>

            <tr>
                <td>Client Name:</td>
                <td>{{ $orderReceipt[0]->name }}</td>
            </tr>
            <tr>
                <td>Table Number:</td>
                <td>RM1</td>
            </tr>
        </table>

        {{-- <hr> --}}
        <br />
        <table class="list-header table-items" style="width: 95%; border-collapse: collapse;">
            <thead style="padding: 10px;">
                <tr>
                <th style="font-size: 15px; text-align: left; border-bottom: 2px solid #000; border-top: 2px solid #000; padding-top: 10px; padding-bottom: 10px" class="list-header-th">Particulars</th>
                <th style="font-size: 15px; width: 60px; border-bottom: 2px solid #000; border-top: 2px solid #000; padding-top: 10px; padding-bottom: 10px" class="text-center list-header-th">Qty</th>
                <th style="font-size: 15px; width: 60px; border-bottom: 2px solid #000; border-top: 2px solid #000; padding-top: 10px; padding-bottom: 10px" class="text-right list-header-th">Price</th>
                <th style="font-size: 15px; width: 110px; border-bottom: 2px solid #000; border-top: 2px solid #000; padding-top: 10px; padding-bottom: 10px" class="text-right  pr-0 list-header-th">Sub Total</th>
                </tr>
            </thead>
            <tbody>
                @foreach($orderReceipt[0]->orderdetail as $item)
                <tr style="padding: 2px; border: none">
                    <td style="font-size: 15px; word-wrap: break-word; padding: 2px 5px; text-transform: uppercase; border: none">{{ $item->product->name }}</td>
                    <td style="font-size: 15px; width:20%; padding: 2px 5px; border: none" class="text-center">{{ $item->quantity }}</td>
                    <td style="font-size: 15px; width:25%; padding: 2px 5px; border: none" class="text-right">{{ \App\Http\Controllers\admin\RecieptController::currencyFormatter($item->unitprice) }}</td>
                    <td style="font-size: 15px; width:25%; padding: 2px 5px; border: none" class="text-right">{{ \App\Http\Controllers\admin\RecieptController::currencyFormatter($item->quantity * $item->unitprice) }}</td>
                </tr>
                @endforeach
                <tr>
                    <td colspan="4" style="border: none;border-bottom: 2px solid #000; padding-bottom: 10px; height: 1px;"></td>
                </tr>
                <tr style="border-top: 1px solid; margin-top: 12px">
                    <td colspan="3" class="text-left" style="font-size: 17px; border: none;">Sub-Total</td>
                    <td class="text-right" style="font-size: 17px; border: none;">{{ \App\Http\Controllers\admin\RecieptController::currencyFormatter($orderReceipt[0]->orderdetail->sum('amount')) }}</td>
                </tr>
                <tr style="border-top: 1px solid">
                    <td colspan="3" class="text-left" style="font-size: 17px;border: none">CGST@6%</td>
                    <td class="text-right" style="font-size: 17px; border: none">{{ \App\Http\Controllers\admin\RecieptController::currencyFormatter($orderReceipt[0]->orderdetail->sum('amount') * 6 / 100) }}</td>
                </tr>
                <tr style="border-top: 1px solid">
                    <td colspan="3" class="text-left" style="font-size: 17px;border: none">SGST@6%</td>
                    <td class="text-right" style="font-size: 15px; border: none">{{ \App\Http\Controllers\admin\RecieptController::currencyFormatter($orderReceipt[0]->orderdetail->sum('amount') * 6 / 100) }}</td>
                </tr>
                <tr style="border-top: 1px solid">
                    <td colspan="3" class="text-left" style="font-size: 17px;border: none">Total Tax</td>
                    <td class="text-right" style="font-size: 15px; border: none">{{ \App\Http\Controllers\admin\RecieptController::currencyFormatter($orderReceipt[0]->orderdetail->sum('amount') * 12 / 100) }}</td>
                </tr>
                <tr style="border-top: 1px solid">
                    <td colspan="3" class="text-left" style="font-size: 17px;border: none">Discount</td>
                    <td class="text-right" style="font-size: 15px; border: none">{{ \App\Http\Controllers\admin\RecieptController::currencyFormatter($orderReceipt[0]->orderdetail->sum('amount') * 12 / 100) }}</td>
                </tr>
                <tr style="border-top: 1px solid">
                    <td colspan="3" class="text-left" style="font-size: 17px;border: none">Round-Off</td>
                    <td class="text-right" style="font-size: 15px; border: none">{{ \App\Http\Controllers\admin\RecieptController::currencyFormatter($orderReceipt[0]->orderdetail->sum('amount') * 12 / 100) }}</td>
                </tr>
                <tr style="border-top: 1px solid">
                    <td colspan="3" class="text-left" style="font-size: 17px;border: none"><b>Total</b></td>
                    <td class="text-right" style="font-size: 15px; border: none"><b>{{ \App\Http\Controllers\admin\RecieptController::currencyFormatter($orderReceipt[0]->orderdetail->sum('amount') + ($orderReceipt[0]->orderdetail->sum('amount') * 12 / 100)) }}</b></td>
                </tr>
            </tbody>
        </table>
        <p style="font-size: 18px; text-align: left;">Amount in words : {{ \App\Http\Controllers\admin\RecieptController::getAmountInWords($orderReceipt[0]->orderdetail->sum('amount') + ($orderReceipt[0]->orderdetail->sum('amount') * 12 / 100))  }} </p>
        {{-- <hr> --}}
    </div>
    <br />
    <h4 style="font-size:18px">
        <p>Don't forget to rate us!<br />
        Scan the QR Code to let us know how you enjoyed with us.</p>
    </h4>
    <br />
    <br />
    <img src="data:image/svg+xml;base64,{{base64_encode($orderReceipt[1])}}"/>
    <br />
    <br />
    <footer style=" text-align: center">
        {{-- <hr /> --}}
        <p style="font-size: 20px">********** Thanks For Visit ** Visit Again **********</p>
        <p style="font-size: 16px">**This is a system generated Slip**</p>
        <p style="font-size: 16px">(Note:- Kindly follow all the guidelines given by uttarakhand Government for the
            purpose of safety from Covid-19)**</p>
    </footer>
</body>

</html>
