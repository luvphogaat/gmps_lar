<x-app-layout>
    <x-slot name="header"></x-slot>
    <div class="container-fluid">
        <form action="{{ route('order.store') }}" method="post">
        @csrf
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header purple text-white">
                        <h3>Order</h3>
                    </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table tableed table-responsive text-nowrap">
                                    <thead class=" text-primary">
                                        <th></th>
                                        <th>
                                            Product Name
                                        </th>
                                        <th>
                                            Qty
                                        </th>
                                        <th>
                                            Price
                                        </th>
                                        <!-- <th>
                                        Dis (%)
                                    </th> -->
                                        <th>
                                            Total
                                        </th>
                                        <th>
                                            <a href="#" class="btn btn-success add_more">
                                                <i class="material-icons">add</i>
                                            </a>
                                        </th>
                                    </thead>
                                    <tbody class="addMoreProduct">
                                        <tr>
                                            <td>1</td>
                                            <td>
                                                <div class="form-outline">
                                                    <select name="product[]" id="product_id"
                                                        class="form-control product_id ">
                                                        <option value="" selected>Select Item</option>
                                                        @foreach( $products as $product )
                                                        <option value="{{ $product->id }}"
                                                            data-price="{{$product->price}}">
                                                            {{ $product->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-outline">
                                                    <input type="number" name="quantity[]" id="quantity"
                                                        class="form-control quantity" value="1" />
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-outline">
                                                    <input type="number" name="price[]" id="price"
                                                        class="form-control price" />
                                                </div>
                                            </td>
                                            <!-- <td >
                                            <div class="form-outline">
                                                <input type="number" name="discount[]" id="discount" class="form-control discount" />
                                            </div>
                                        </td> -->
                                            <td>
                                                <div class="form-outline">
                                                    <input type="number" name="total_amount[]" id="total_amount"
                                                        class="form-control total_amount" />
                                                </div>
                                            </td>
                                            <td>
                                                <a href="#" class="btn btn-danger delete">
                                                    <i class="material-icons">close</i>
                                                </a>
                                            </td>
                                        </tr>
                                        {{-- @foreach ( as ) --}}

                                        {{-- @endforeach --}}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header purple text-white">
                        <h3>Total: <b class="total">0.00</b></h3>
                    </div>
                    <div class="card-body">
                        <div class="conatiner-fluid">
                            <div class="form-group">
                                <label class="" for="room_id">Select Room</label>
                                <select name="room_id" id="table_id" class="form-control table_id "
                                    data-style="btn btn-link" required>
                                    <option value="" selected></option>
                                    @foreach( $rooms as $room )
                                    <option value="{{ $room->id }}">{{ $room->room_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="paystatus_id">Example select</label>
                                <select class="form-control paystatus_id" data-style="btn btn-link" id="paystatus_id"
                                    required>
                                    <option value="0">Unpaid</option>
                                    <option value="1">Paid</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group bmd-form-group" style="width: 100%">
                            <button type="submit" class="btn btn-primary" style="width: 100%">SAVE<div
                                    class="ripple-container"></div>
                            </button>
                            <button type="button" class="btn btn-info" style="width: 100%" onclick="PrintReceiptContent('print')">
                                Print<div class="ripple-container"></div>
                            </button>
                        </div>
                        <section id="paying-status" style="display: none">
                            <div class="form-group">
                                <label for="paystatus_id">Payment Type</label>
                                <select class="form-control paystatus_id" data-style="btn btn-link" id="paystatus_id"
                                    required>
                                    <option value="cash" selected>Cash</option>
                                    <option value="paytm">Paytm</option>
                                    <option value="googlepay">Google Pay</option>
                                </select>
                            </div>
                            <div class="form-group bmd-form-group">
                                <label class="bmd-label-floating">Payment Id</label>
                                <input type="text" class="form-control" name="paid_id" id="paid_id">
                            </div>
                            <div class="form-group bmd-form-group">
                                <label class="bmd-label-floating">Pay Amount</label>
                                <input type="number" class="form-control" name="paid_amount" id="paid_amount">
                            </div>
                            <div class="form-group ">
                                <label class="">Returning Change</label>
                                <input type="number" class="form-control" name="balance" id="balance" readonly>
                            </div>
                            <div class="form-group bmd-form-group" style="width: 100%">
                                <button type="submit" class="btn btn-primary" style="width: 100%">PAY<div
                                        class="ripple-container"></div>
                                </button>
                                <!-- <button type="submit" class="btn btn-primary"><div class="ripple-container"></div></button> -->
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </form>
    </div>
    <div class="modal">
        <div id="print">
            {{-- @include('modules.reports.receipt') --}}
        </div>
    </div>
    @section('script')
    <script>
        $(document).ready(function () {
            $('.add_more').on('click', function () {
                var product = $('.product_id').html();
                var numberofrow = ($('.addMoreProduct tr').length - 0) + 1;
                var tr = `<tr><td class="no">${numberofrow}</td>
                <td><select class="form-control product_id" name="product_id[]">${product}</select></td>
                <td><input type="number" name="quantity[]"  class="form-control quantity" value="1" /></td>
                <td><input type="number" name="price[]" class="form-control price" /></td>
                <td><input type="number" name="total_amount[]" class="form-control total_amount " /></td>
                <td><a href="#"  class="btn btn-danger delete"><i class="material-icons">close</i></a></td></tr>`;
                $('.addMoreProduct').append(tr);
            });

            $('.addMoreProduct').delegate('.delete', 'click', function () {
                debugger;
                if ($(this).parent().parent().parent().find('tr').length > 1) {
                    $(this).parent().parent().remove();
                }
            });

            function TotalAmount() {
                var total = 0;
                $('.total_amount').each(function (i, e) {
                    var amount = $(this).val() - 0;
                    total += amount;
                });

                $('.total').html(total);
            }

            $('.addMoreProduct').delegate('.product_id', 'change', function () {
                var tr = $(this).parent().parent().parent();
                var price = tr.find('.product_id option:selected').attr('data-price');
                tr.find('.price').val(price);
                var qty = tr.find('.quantity').val() - 0;
                var disc = 0;
                var price = tr.find('.price').val() - 0;
                var total_amount = (qty * price) - ((qty * price * disc) / 100);
                tr.find('.total_amount').val(total_amount);
                TotalAmount();
            });

            $('.addMoreProduct').delegate('.quantity', 'keyup', function () {
                var tr = $(this).parent().parent().parent().parent();
                var qty = tr.find('.quantity').val() - 0;
                // var disc = tr.find('.discount').val() - 0;
                var disc = 0;
                var price = tr.find('.price').val() - 0;
                var total_amount = (qty * price) - ((qty * price * disc) / 100);
                tr.find('.total_amount').val(total_amount);
                TotalAmount();
            });

            $('#paid_amount').keyup(function () {
                // alert(1);
                var total = $('.total').html();
                var paid_amount = $(this).val();
                var tot = paid_amount - total;
                $('#balance').val(tot).toFixed(2);
            });

            //  Print Section

        })
        function PrintReceiptContent(el) {
                var data = `<input type="button" id="printPageButton" class="printPageButton" style="display:block; width: 100%; border: none; background-color: #008B8B; color: #fff; padding: 14px 28px; font-size: 16px; cursor: pointer; text-align: center;" value="Print Receipt" onclick="window.print()"`;
                data += document.getElementById(el).innerHTML;
                var myReceipt =  window.open("", "myWin", "left=150, top=130, width=400, height=400");
                myReceipt.screenX = 0;
                myReceipt.screenY = 0;
                myReceipt.document.write(data);
                myReceipt.document.title = 'Print Receipt';
                myReceipt.focus();
                setTimeout(() => {
                        // myReceipt.close();
                }, 8000);

            }


    </script>
    @endsection
</x-app-layout>
