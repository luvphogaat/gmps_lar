<x-app-layout>
    <x-slot name="header"></x-slot>
    <style>
        table, th, td {
            /* min-width: 145px; */
            border:1px solid;
            border-collapse: collapse;
        }
        th, td {
            padding: 15px;
        }
        .custom-select-css {
            background-image: none !important;
            border: 1px solid #d2d2d2 !important;
            height: 50px !important;
            padding: 10px !important;
        }
        .custom-text-css {
            background-image: none !important;
            border: 1px solid #d2d2d2 !important;
            height: 35px !important;
        }
    </style>
    @livewire('orders.new-order', ['order_number' => $order_number])
    @section('script')
    <script>
    </script>
    @endsection
</x-app-layout>
