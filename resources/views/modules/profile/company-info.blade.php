<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Company Info') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
                @endif
                    <form method="POST" action="{{ route('company-info.update', $companyinfo->id) }}">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Company Name</label>
                                    <input type="text" class="form-control" value="{{ $companyinfo->company_name }}" required name="company_name">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Gst Charges(%)</label>
                                    <input type="number" class="form-control" value="{{ $companyinfo->gst }}" name="gst">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="bmd-label-floating">State gst Charges(%)</label>
                                    <input type="number" class="form-control" value="{{ $companyinfo->sgst }}" name="sgst">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Central gst Charges(%)</label>
                                    <input type="number" class="form-control" value="{{ $companyinfo->cgst }}" name="cgst">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Address</label>
                                    <input type="text" class="form-control" required value="{{ $companyinfo->address }}" name="address">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Phone Number</label>
                                    <input type="tel" class="form-control" required value="{{ $companyinfo->phone }}" name="phone">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="bmd-label-floating">State</label>
                                    <input type="text" class="form-control" value="{{ $companyinfo->state }}" name="state" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Country</label>
                                    <input type="text" class="form-control" value="{{ $companyinfo->country }}" name="country" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Currency</label>
                                    <input type="text" class="form-control" value="{{ $companyinfo->currency  }}" name="currency" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Message</label>
                                    <input type="text" class="form-control" value="{{ $companyinfo->message  }}" name="message">
                                </div>
                            </div>
                            <button class="btn btn-info" type="submit">Save Changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
