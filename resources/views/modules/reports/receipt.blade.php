<div>
<div id="invoice-pos">
    <div id="printed_content">
        <center id="top">
            <div class="logo">Logo</div>
            <div class="info"></div>
            <h2>Ganga Cottage</h2>
        </center>
    </div>
    <div class="mid">
        <div class="info">
            <h2>Contact Us</h2>
            <p>
                Addres::
                Email::
                Phone::
            </p>
        </div>
    </div>


    <div class="bot">
        <div id="table">
            {{ dd($order_receipt) }}
            <table>
                <tr class="tabletitle">
                    <td class="item"><h2>Items</h2></td>
                    <td class="Hours"><h2>Qty</h2></td>
                    <td class="Rate"><h2>Unit</h2></td>
                    <td class="Rate"><h2>Sub Total</h2></td>
                </tr>
                @foreach($order_receipt as $receipt)
                    <tr class="service">
                        <td class="tableitem"><p class="itemtext">{{ $receipt->product->name }}</p></td>
                        <td class="tableitem"><p class="itemtext">{{ $receipt->quantity }}</p></td>
                        <td class="tableitem"><p class="itemtext">{{ number_format($receipt->unitprice, 2) }}</p></td>
                        <td class="tableitem"><p class="itemtext">{{  number_format($receipt->amount, 2) }}</p></td>
                    </tr>
                @endforeach


                <tr class="tabletitle">
                    <td class=""></td>
                    <td class=""></td>
                    <td class="Rate"><p class="itemtext"></p>Tax</td>
                    <td class="Payment"><p class="itemtext">$ {{  number_format($receipt->sum('amount'), 2) }}</p></td>
                </tr>
                <tr class="tabletitle">
                    <td class=""></td>
                    <td class=""></td>
                     <td class="Rate">Total</td>
                    <td class="Payment"><h2>{{ number_format($order_receipt->sum('amount'), 2) }}</h2></td>
                </tr>
            </table>
            <div class="legalcopy">
                <p class="legal"><strong>
                        ** Thank you for visiting **
                    </strong><br>

                </p>
            </div>
        </div>
    </div>
</div>
</div>
<style>
    #invoice-pos {
        box-shadow: 0 0 1in -0.25in rgb(0, 0, 0.5);
        padding: 2mm;
        margin: 0 auto;
        width: 58mm;
        background: #fff;
    }

    #invoice-pos ::selection {
        background: #34495E;
        color: #fff;
    }

    #invoice-pos ::-moz-selection {
        background: #34495E;
        color: #fff;
    }

    #invoice-pos h1 {
        font-size: 1.5em;
        color: #222;
    }

    #invoice-pos h2 {
        font-size: 0.5em;
        color: #222;
    }

    #invoice-pos h3 {
        font-size: 1.2em;
        font-weight: 300 line-height: 2em;

    }

    #invoice-pos p{
        font-size: 0.7em;
        line-height: 1.2em;
        color: #666;
    }

    #invoice-pos #top, #invoice-pos #mid, #invoice-pos #bot {
        border-bottom: 1px solid #eee;
    }

    #invoice-pos #top {
        min-height: 100px;
    }
    #invoice-pos #mid {
        min-height: 80px;
    }
    #invoice-pos #bot {
        min-height: 50px;
    }
    #invoice-pos #top .logo {
        height: 60px;
        width: 60px;
        background-image: url('') no-repeat;
        background-size: 60px 60px;
        border-radius: 50px;
    }
    #invoice-pos .info {
        display: block;
        margin-left: 0;
        text-align: center;
    }

    #invoice-pos .title {
        float: right;
    }
    #invoice-pos .title p {
        text-align: right;
    }

    #invoice-pos table{
        width: 100%;
        border-collapse: #eee;
    }
    #invoice-pos .tabletitle {
        font-size: 0.5em;
        background: #eee;
    }
    #invoice-pos .service {
        border-bottom: 1px solid #eee;
    }
    #invoice-pos .item {
        width: 24mm;
    }
    #invoice-pos .itemtext {
        font-size: 0.5em;
    }

    #invoice-pos #legalcopy {
        margin-top: 5mm;
        text-align: center;

    }
    #invoice-pos .serial-number {
        margin-top: 5mm;
        margin-bottom: 2mm;
        text-align: center;
        font-size: 12px;
    }

    .serial {
        font-size:  10px !important;
    }

</style>
