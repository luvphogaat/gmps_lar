<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Reservation List') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                    @endif
                    <table>
                        <thead class=" text-primary">
                            <tr>
                                <th></th>
                                <th>Name</th>
                                <th class="">Room Name</th>
                                <th class="">Check In</th>
                                <th class="">Check Out</th>
                                <th class="">No of Nights</th>
                                <th class="">Advance Amount</th>
                                <th class="">Booking Source</th>
                                <th class="">#</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($reservationList as $key => $reservation)
                            <tr>
                                <td>
                                    {{ $key + 1 }}
                                </td>
                                <td>
                                    {{ $reservation->guest->name }}
                                </td>
                                <td>
                                    {{-- @foreach($reservation->room as $room) --}}
                                        {{ $reservation->room[0]->room_type }}
                                    {{-- @foreach --}}
                                </td>
                                <td class="">
                                    {{ $reservation->checkin }}
                                </td>
                                <td class="">
                                    {{ $reservation->checkout }}
                                </td>
                                <td class="">
                                    {{ $reservation->nights }}
                                </td>
                                <td>
                                    {{ $reservation->advance_amount }}
                                </td>
                                <td style="text-transform: uppercase">
                                    {{ $reservation->source }}
                                </td>
                                <td class="">
                                    {{-- <a class="btn btn-primary" href="{{ route('dashboard.receipt.reservation.show', $reservation->id) }}"
                                        role="button">Edit</a> --}}
                                    <a class="btn btn-primary" target="_blank" href="{{ route('dashboard.receipt.reservation.show', $reservation->id) }}"
                                        role="button">View</a>
                                        <a class="btn btn-success" target="_blank" href="{{ route('dashboard.receipt.room.bill', $reservation->reservation_number) }}"
                                            role="button">Print Bill</a>
                                    @permission('tariffs-delete')
                                    {{-- <button type="button" class="btn btn-sm btn-danger"
                                        onclick="event.preventDefault();document.getElementById('delete-user-form-{{$reservation->id}}').submit();">
                                        Delete
                                    </button>
                                    <form id="delete-user-form-{{$reservation->id}}"
                                        action="{{route('tariff.destroy', $reservation->id )}}" method="POST"
                                        style="display:none;">
                                        @csrf
                                        @method("DELETE")
                                    </form> --}}
                                    @endpermission
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
