<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('New Reservation') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-12xl mx-auto sm:px-12 lg:px-12">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg" style="padding: 20px">
                <button class="btn btn-primary btn-round" style="width:100%" id="add_more_rooms">ADD ROOM</button>
                <form class="col s12" action="{{ route('dashboard.reservation.create') }}" method="post">
                    @csrf
                    <div id="customer_room_details">
                        <div style="border:3px solid; padding: 20px" class="resercation_section">
                            <div class="row">
                                <div class="col s12 md3">
                                    <div class="form-group">
                                        <label for="icon_prefix">Name</label>
                                        <input id="icon_prefix" type="text" name="name[]" required value="{{ old('name') }}">
                                    </div>
                                </div>
                                <div class="col s12 md3">
                                    <div class="form-group">
                                        <label for="icon_email">Email</label>
                                        <input id="icon_email" type="email" name="email[]" required
                                            value="{{ old('email') }}">
                                    </div>
                                </div>
                                <div class="col s12 md3">
                                    <div class="form-group">
                                        <label for="icon_telephone">Telephone</label>
                                        <input id="icon_telephone" type="tel" class="validate" name="phone[]" required
                                            value="{{ old('phone') }}">
                                    </div>
                                </div>
                                <div class="col s12 md3">
                                    <div class="form-group">
                                        <label for="icon_guestCount">No of Adults</label>
                                        <input id="icon_guestCount" type="number" min="1" max="12" class="validate"
                                            name="adultCount[]" required value="{{ old('adultNumber') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12 md3">
                                    <div class="form-group">
                                        <label for="icon_guestCountChild">No of Childrens</label>
                                        <input id="icon_guestCountChild" type="number" min="1" max="12" class="validate"
                                            name="childCount[]" required value="{{ old('childNumber') }}">
                                    </div>
                                </div>
                                <div class="col s12 md3">
                                    <div class="form-group">
                                        <label for="icon_guestCheckin">Check In Date</label>
                                        <input id="icon_guestCheckin" type="date" class="validate" name="checkin[]" required
                                            value="{{ old('guestNumber') }}">
                                    </div>
                                </div>
                                <div class="col s12 md3">
                                    <div class="form-group">
                                        <label for="icon_guestCheckout">Check Out Date</label>
                                        <input id="icon_guestCheckout" type="date" class="validate" name="checkout[]" required
                                            value="{{ old('guestNumber') }}">
                                    </div>
                                </div>
                                <div class="col s12 md3">
                                    <div class="form-group">
                                        <label for="icon_guestSource">Source</label>
                                        <select name="source[]" id="icon_guestSource" class="form-control" required>
                                            <option value="direct" selected>DIRECT</option>
                                            <option value="goibibo">GOIBIBO</option>
                                            <option value="mmt">MMT</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12 md3">
                                    <div class="form-group">
                                        <label class="" for="table_id">Select Room</label>
                                        <select name="room[]" id="table_id" class="form-control" required
                                            style="height:3.1rem;">
                                            <option value="" selected></option>
                                            @foreach( $roomList as $room )
                                            <option value="{{ $room->id }}">{{ $room->room_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col s12 md3">
                                    <div class="form-group">
                                        <label for="icon_guestpayment">Amount</label>
                                        <input id="icon_guestpayment" type="number" class="validate"
                                            name="total_amount[]" required value="{{ old('guestNumber') }}">
                                    </div>
                                </div>
                                <div class="col s12 md3">
                                    <div class="form-group">
                                        <label for="icon_guestpayment">Sell Amount</label>
                                        <input id="icon_guestpayment" type="number" class="validate"
                                            name="sell_amount[]" required value="{{ old('guestNumber') }}">
                                    </div>
                                </div>
                                <div class="col s12 md3">
                                    <div class="form-group">
                                        <label for="icon_guestpayment">Advance Payment</label>
                                        <input id="icon_guestpayment" type="number" class="validate"
                                            name="advance_amount[]" required value="{{ old('guestNumber') }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br />
                    </div>
                    <div style="padding: 20px">
                        <div class="row book-button">
                            <div class="col s4">
                                <button class="btn btn-primary btn-round" style="width:100%">BOOK</button>
                            </div>
                            <div class="col s4">
                                <button class="btn btn-primary btn-round" style="width:100%">RESET</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @section('script')
    <script>
        $(document).ready(function () {
            $('#add_more_rooms').on('click', function () {
                var roomDiv = '';
                $('.resercation_section').clone().appendTo('#customer_room_details');

            });
            $('.customer_room_details').delegate('.delete', 'click', function () {
                if ($(this).parent().parent().parent().find('div').length > 1) {
                    $(this).parent().parent().remove();
                }
            });
        });
    </script>
    @endsection
</x-app-layout>
