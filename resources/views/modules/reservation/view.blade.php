<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Reservation</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

        <style type="text/css" media="screen">
            html {
                font-family: Arial, Helvetica, sans-serif;
                line-height: 1.15;
                margin: 0;
            }

            body {
                font-family: Arial;
                font-weight: 400;
                line-height: 1.5;
                color: #212529;
                text-align: left;
                background-color: #fff;
                font-size: 12px;
                margin: 36pt;
            }

            h4 {
                margin-top: 0;
                margin-bottom: 0.5rem;
            }

            p {
                margin-top: 0;
                margin-bottom: 1rem;
            }

            strong {
                font-weight: bolder;
            }

            img {
                vertical-align: middle;
                border-style: none;
            }

            table {
                border-collapse: collapse;
            }

            th {
                text-align: inherit;
            }

            h4, .h4 {
                margin-bottom: 0.5rem;
                font-weight: 500;
                line-height: 1.2;
            }

            h4, .h4 {
                font-size: 1.5rem;
            }

            .table {
                width: 100%;
                margin-bottom: 1rem;
                color: #212529;
            }

            .table th,
            .table td {
                padding: 0.75rem;
                vertical-align: top;
            }

            .table.table-items td {
                /* border-top: 1px solid #dee2e6; */
            }

            .table thead th {
                vertical-align: bottom;
                /* border-bottom: 2px solid #dee2e6; */
            }

            .mt-5 {
                margin-top: 3rem !important;
            }

            .pr-0,
            .px-0 {
                padding-right: 0 !important;
            }

            .pl-0,
            .px-0 {
                padding-left: 0 !important;
            }

            .text-right {
                text-align: right !important;
            }

            .text-center {
                text-align: center !important;
            }

            .text-uppercase {
                text-transform: uppercase !important;
            }
            * {
                font-family: "DejaVu Sans";
            }
            body, h1, h2, h3, h4, h5, h6, table, th, tr, td, p, div {
                line-height: 1.1;
            }
            .party-header {
                font-size: 1.5rem;
                font-weight: 400;
            }
            .total-amount {
                font-size: 12px;
                font-weight: 700;
            }
            .border-0 {
                border: none !important;
            }
            .cool-gray {
                color: #6B7280;
            }
            .table-border {
                /* border: solid 1px black; */
                /* border-radius: 4px; */
                /* overflow: hidden; */
                /* text-align: left; */
                width: 100%;
                border: 1px solid black;
                /* border-collapse: collapse; */
            }
            .table-border td, th {
                border: 1px solid black;
                padding: 8px 15px;
            }
        </style>
    </head>

    <body>
        {{-- {{ dd($reservationList) }} --}}
        {{-- Header --}}
        {{-- <img src="https://www.gangacottage.com/images/logo_new/GC_New_1.png" alt="logo" height="100"> --}}
        {{-- <div class="text-uppercase" style="text-align: right; position: absolute; right:0px; border:double;">
            <h1><strong style="color: #274e14;">GANGA COTTAGE</strong></h1>
            <p>Pastora Par Near Binsar Mahadev Mandir,</p>
            <p> Ranikhet, Uttarakhand - 263664</p>
            <p>09119034598</p>
        </div>
        <div style="border:double">Hello</div> --}}
        {{-- {{ dd($reservationList) }} --}}
        <table class="table mt-0">
            <tbody>
                <tr>
                    <td style="width:40%">
                        <img src="https://www.gangacottage.com/images/logo_new/GC_New_1.png" alt="logo" height="100">
                    </td>
                    <td style="text-align: right">
                        <h1>
                            <strong style="color: #274e14;font-weight: bolder;font-size: 40px;font-family: 'Times New Roman', Times, serif; text-transform: uppercase;">
                                {{ $companyInfo->company_name }}
                            </strong>
                        </h1>
                        <span>{{ $companyInfo->address }}</span><br/>
                        <span>{{ $companyInfo->phone }}</span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>
                            <strong>GSTIN No:</strong> 05ESDPP1862E1ZU
                        </p>
                    </td>
                    <td style="text-align: right">
                        <p>
                            <strong>Date:</strong> {{ date('d-m-Y', strtotime($reservationList->created_at)) }}
                        </p>
                    </td>
                </tr>
            </tbody>
        </table>
        <hr/>
        <h3 style="text-align: center; text-decoration: underline;">
            HOTEL RESERVATION LETTER
        </h3>
        <p>
            Dear {{  $reservationList->guest->name }},
        </p>
        <p>
            I would like to confirm your booking for the reservation in our hotel “Ganga Cottage” for the date {{ date('dS F Y', strtotime($reservationList->checkin)) }} till {{ date('dS F Y', strtotime($reservationList->checkout)) }} for {{ $reservationList->nights }} nights.
        </p>
        <h4 style="text-decoration: underline; font-size: 12px;">
            <strong>Reservation Details</strong>
        </h4>
        <table class="table-border">
            <tr>
                <td style="width:30%">
                    Reservation Number
                </td>
                <td>
                    {{ sprintf('%06d', $reservationList->reservation_number) }}
                </td>
            </tr>
            <tr>
                <td>
                    Room
                </td>
                <td>
                    {{ $reservationList->room[0]['room_type'] }}
                </td>
            </tr>
            <tr>
                <td>
                    Arrival Date
                </td>
                <td>
                    {{ date('dS F Y', strtotime($reservationList->checkin)) }}
                </td>
            </tr>
            <tr>
                <td>
                    Departure Date
                </td>
                <td>
                    {{ date('dS F Y ', strtotime($reservationList->checkout)) }}
                </td>
            </tr>
            <tr>
                <td>
                    Number of Nights
                </td>
                <td>
                    {{ $reservationList->nights }}
                </td>
            </tr>
            <tr>
                <td>
                    No. of Adults/Child
                </td>
                <td>
                    {{ $reservationList->adultCount }} / {{ $reservationList->childCount }}
                </td>
            </tr>
            <tr>
                <td>
                    Amenities
                </td>
                <td>
                    Free Wifi, Parking, etc
                </td>
            </tr>
            <tr>
                <td>
                    Amount Recd
                </td>
                <td>
                    {{ $reservationList->advance_amount ? $reservationList->advance_amount : 'NILL'  }}
                </td>
            </tr>
        </table>
        <h4 style="text-decoration: underline; font-size: 12px;" class="mt-5">
            <strong>Guest Details</strong>
        </h4>
        <table class="table-border">
            <tr>
                <td style="width:30%">
                    Name
                </td>
                <td>
                    {{  $reservationList->guest->name }}
                </td>
            </tr>
            <tr>
                <td>
                    Contact
                </td>
                <td>
                    {{  $reservationList->guest->contact }}
                </td>
            </tr>
            <tr>
                <td>
                    Email Id
                </td>
                <td>
                    {{  $reservationList->guest->email }}
                </td>
            </tr>
        </table>

        <footer style="position: absolute;bottom:10px; text-align: center">
            <p style="font-size: 9px">**This is a system generated Slip**</p>
            <p style="font-size: 9px">(Note:- Kindly follow all the guidelines given by uttarakhand Government for the purpose of safety from Covid-19)**</p>
        </footer>

        <script type="text/php">
            if (isset($pdf) && $PAGE_COUNT > 1) {
                $text = "Page {PAGE_NUM} / {PAGE_COUNT}";
                $size = 10;
                $font = $fontMetrics->getFont("Verdana");
                $width = $fontMetrics->get_text_width($text, $font, $size) / 2;
                $x = ($pdf->get_width() - $width);
                $y = $pdf->get_height() - 35;
                $pdf->page_text($x, $y, $text, $font, $size);
            }
        </script>
    </body>
</html>
