<x-app-layout>
    <x-slot name="header">
        {{-- <link href="http://demo.expertphp.in/css/jquery.ui.autocomplete.css" rel="stylesheet"> --}}
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Stocks List') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form action="{{ route('stocks.store') }}" method="post">
                        @csrf
                        {{-- {!! Form::text('search_text', null, array('placeholder' => 'Search Text','class' => 'form-control','id'=>'search_text')) !!} --}}
                        {{-- <input type="text" placeholder="Search Text" class="form-control" id="search_text" /> --}}
                        <div class="form-group">
                            <label for="paystatus_id">Stock Name</label>
                            <input id="seed_one" type="text" name="stock_name"/>
                        </div>
                        <div class="form-group">
                            <label for="paystatus_id">Stock Brand</label>
                            <input type="text" name="brand"/>
                        </div>
                        <div class="form-group">
                            <label for="paystatus_id">Stock Price</label>
                            <input type="number" min="0.00" max="10000.00" step="0.01" name="price"/>
                        </div>
                        <div class="form-group">
                            <label for="paystatus_id">Stock Quantity</label>
                            <input type="number" name="quantity"/>
                        </div>
                        <button type="submit" class="btn btn-primary" style="width: 100%">SAVE<div
                            class="ripple-container"></div>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @section('script')
    <script src="https://code.jquery.com/ui/1.13.0/jquery-ui.min.js" integrity="sha256-hlKLmzaRlE8SCJC1Kw8zoUbU8BxA+8kR3gseuKfMjxA=" crossorigin="anonymous"></script>
<script >
     $(document).ready(function(){
        src = "{{ route('searchajax') }}";
        $( "#seed_one" ).autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: src,
                    dataType: "json",
                    data: {
                        term: request.term
                    },
                    success: function (data) {
                        response(data);

                    }
                });
            },
            minLength: 3,
            select: function( event, ui ) {
                // Your autoComplete response returns the ID in the 'value' field
                // window.location = 'http://yoursite.com/products/' + ui.item.value
            }
        });
    });
 </script>
 @endsection
</x-app-layout>

