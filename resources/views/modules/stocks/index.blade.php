<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Stocks List') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <a href="{{ route('stocks.create') }}" class="btn btn-primary" style="float: right">ADD NEW STOCK<div
                class="ripple-container"></div>
            </a>
            <div class="bg-white shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                    @endif
                    <table>
                        <thead class=" text-primary">
                            <tr>
                                <th></th>
                                <th>Name</th>
                                <th class="">Brand</th>
                                <th class="">Price</th>
                                <th class="">Quantity</th>
                                <th class="">#</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($stockList as $stock)
                            <tr>
                                <td></td>
                                <td>{{ $stock->stock_name }}</td>
                                <td>{{ $stock->brand }}</td>
                                <td>{{ $stock->price }}</td>
                                <td>{{ $stock->quantity }}</td>
                                <td>#</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
