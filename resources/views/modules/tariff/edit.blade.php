<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Edit Room Tariff') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form method="POST" action="{{ route('tariff.update', $tariff->id) }}">
                        @csrf
                        @method('PUT')
                        <div class="mb-3">
                            <label for="name" class="form-label">Tariff Name</label>
                            <input name="tariff_name" type="text"
                                class="form-control @error('tariff_name') is-invalid  @enderror" id="tariff_name"
                                aria-describedby="tariff_name" value="{{ $tariff->tariff_name }}">
                            @error('tariff_name')
                            <span class="invalid-feedback" role="alert">
                                {{ $message }}
                            </span>
                            @enderror
                        </div>

                        <div class="mb-3">
                            <label for="price" class="form-label">Discounted Price</label>
                            <input name="price" type="number"
                                class="form-control @error('price') is-invalid  @enderror"
                                id="price" aria-describedby="price"
                                value="{{ $tariff->price }}">
                            @error('price')
                            <span class="invalid-feedback" role="alert">
                                {{ $message }}
                            </span>
                            @enderror
                        </div>

                        <div class="mb-3">
                            <label for="cut_price" class="form-label">Non-Discounted Proce</label>
                            <input name="cut_price" type="number"
                                class="form-control @error('cut_price') is-invalid  @enderror" id="cut_price"
                                aria-describedby="cut_price" value="{{ $tariff->cut_price }}">
                            @error('cut_price')
                            <span class="invalid-feedback" role="alert">
                                {{ $message }}
                            </span>
                            @enderror
                        </div>

                        <div class="mb-3">
                            <label for="noofrooms" class="form-label">No of Rooms</label>
                            <input name="noofrooms" type="number"
                                class="form-control @error('noofrooms') is-invalid  @enderror" id="noofrooms"
                                aria-describedby="noofrooms" value="{{ $tariff->noofrooms }}">
                            @error('noofrooms')
                            <span class="invalid-feedback" role="alert">
                                {{ $message }}
                            </span>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
