<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Room Tariff') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                    @endif
                    <table>
                        <thead class=" text-primary">
                            <tr>
                                <th></th>
                                <th>Name</th>
                                <th class="text-center">Price</th>
                                <th class="text-center">Non Discounted Price</th>
                                <th class="text-center">No of Rooms</th>
                                <th class="text-center">#</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($tariffList as $tariff)
                            <tr>
                                <td><img src="{{ asset('images'). '/' .$tariff->image }}" width="50" height="50" /></td>
                                <td>{{ $tariff->tariff_name }}</td>
                                <td class="text-center">&#x20B9; {{ $tariff->price }}</td>
                                <td class="text-center">&#x20B9; {{ $tariff->cut_price }}</td>
                                <td class="text-center">{{ $tariff->noofrooms }}</td>
                                <td class="text-center">
                                    <a class="btn btn-primary" href="{{ route('tariff.edit', $tariff->id) }}"
                                        role="button">Edit</a>
                                    @permission('tariffs-delete')
                                    <button type="button" class="btn btn-sm btn-danger"
                                        onclick="event.preventDefault();document.getElementById('delete-user-form-{{$tariff->id}}').submit();">
                                        Delete
                                    </button>
                                    <form id="delete-user-form-{{$tariff->id}}"
                                        action="{{route('tariff.destroy', $tariff->id )}}" method="POST"
                                        style="display:none;">
                                        @csrf
                                        @method("DELETE")
                                    </form>
                                    @endpermission
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
