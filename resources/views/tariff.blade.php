@extends('main')
@section('content')
<div class="container">
    <div style="width:100%;height:auto !important;//border:double;">
        <style>
            #featuredlist li {
                background: url("{{ asset('images/right_arrow.png')}}") no-repeat left center;
                height: 35px;
                list-style: none;
                padding-left: 40px;
                line-height: 35px;
                list-style-position: inside;
                font-size: 15px;
                font-family: sans-serif;
            }

            .btn {
                background-color: #174b0e;
            }

            .discount {
                position: absolute;
                right: 0;
                float: right;
                z-index: -100;
            }
        </style>
        <h3 class="center">Package/Pricing</h3>
        <br/>
        <div class="row center">
            @foreach ($tariffs as $tariff) 
            <div class="col s12 m6">
                <div class="card">
                    {{-- <img src="{{ asset('images/discountoff.png')}}" width="70px" height="70px" class="discount" /> --}}
                    <div class="card-image">
                        <img src="{{ asset('images/' . $tariff->image)}}">
                        <span class="card-title" style="background:rgba(0,0,0,0.5);width:100%;">{{ $tariff->tariff_name }} (&#8377;
                            {{ $tariff->price }}/- + taxes)</span>
                    </div>
                    <div class="card-content" style="display:flex">
                        <a class="waves-effect waves-light btn" href="{{ route('inquiry', $tariff->id) }}">Inquire Now</a>
                        <!--        <h6>For Booking Call 8076502209</h6>-->
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection