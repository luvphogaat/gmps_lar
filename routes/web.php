<?php

use App\Http\Controllers\admin\DashboardController;
use App\Http\Controllers\admin\TariffController;
use App\Http\Controllers\admin\OrderController;
use App\Http\Controllers\admin\ReportController;
use App\Http\Controllers\admin\CompanyInfoController;
use App\Http\Controllers\admin\TransactionController;
use App\Http\Controllers\admin\BookingController;
use App\Http\Controllers\admin\RecieptController;
use App\Http\Controllers\admin\ReservationController;
use App\Http\Controllers\StockController;
use App\Http\Controllers\ImageGalleryController;
use App\Http\Controllers\IndexController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
    Route::group(['middleware' => 'HtmlMinifier'], function () {

        Route::get('/', [IndexController::class, 'index'])->name('index');

        Route::get('gallery', [ImageGalleryController::class, 'index'])->name('gallery');

        Route::get('explore', [IndexController::class, 'explore'])->name('explore');

        Route::get('tariff', [IndexController::class, 'tariff'])->name('tariff');

        Route::get('inquiry/{t_id}', [IndexController::class, 'bookingInquery'])->name('inquiry');

        Route::get('contactus', [IndexController::class, 'contactus'])->name('contactus');

        Route::get('thankyou', function () {
            return view('thankyou');
        })->name('thankyou');

        Route::prefix('book')->group(function () {
            Route::get('', [BookingController::class, 'index'])->name('bookingEngine');
            Route::post('user-details', [BookingController::class, 'userDetails'])->name('bookingEngine.user.details');
            Route::get('search', [BookingController::class, ''])->name('bookingEngine.room');
            Route::post('search-room', [BookingController::class, ''])->name('bookingEngine.room.save');
            Route::post('paytm-payment', [BookingController::class, 'paytmPayment'])->name('paytm.payment');
            Route::post('paytm-callback', [BookingController::class, 'paytmCallback'])->name('paytm.callback');
        });
    });


    // Route::get('contactus/reload-captcha', [IndexController::class, 'reloadCaptcha'])->name('contactus.captcha-reload');

    Route::post('contactus/save', [IndexController::class, 'savecontactUsData'])->name('contactus.save');

    Route::post('inquiry/{t_id}/save', [IndexController::class, 'bookingInquerySave'])->name('inquiry.save');

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');

    Route::prefix('dashboard')->middleware(['auth'])->group(function () {
        Route::get('/', [DashboardController::class, 'index'])->name('dashboard');

        // Staff Admin
        Route::middleware(['role:admin'])->group(function () {
            Route::get('product/list', [DashboardController::class, 'productlist'])->name('dashboard.products.list');
            Route::get('transaction', [TransactionController::class, 'index'])->name('dashboard.transaction.index');
            Route::get('transaction/create', [TransactionController::class, 'linkGenerate'])->name('dashboard.transaction.link.create');
            // Route::get('food/receipt/', [RecieptController::class, 'index'])->name('dashboard.receipt.index');

            Route::resource('company-info', CompanyInfoController::class);
            Route::prefix('receipt')->group(function () {
                // Route::get('reservation/{reservationId}', [ReservationController::class, 'show'])->name('dashboard.receipt.reservation.show');

                // Route::get('room/{bookingId}', [RecieptController::class, 'roomshow'])->name('dashboard.receipt.room.show');
            });

            Route::prefix('reservation')->group(function () {
                Route::get('', [ReservationController::class, 'index'])->name('dashboard.reservation.list');
                Route::get('new', [ReservationController::class, 'create'])->name('dashboard.reservation.new');
                Route::post('create', [ReservationController::class, 'store'])->name('dashboard.reservation.create');
                Route::get('{reservationId}', [ReservationController::class, 'show'])->name('dashboard.receipt.reservation.show');
                Route::get('room/receipt/{id}', [RecieptController::class, 'roomshow'])->name('dashboard.receipt.room.bill');
            });

            Route::resource('stocks', StockController::class);
        });

        // Staff Manager
        Route::middleware(['role:manager'])->group(function (){

        });

        // Staff Routes
        Route::middleware(['role:staff'])->group(function (){

        });

        Route::middleware(['role:admin|manager'])->group(function() {
            Route::get('gallery', [ImageGalleryController::class, 'index'])->name('dashboard.gallery.view');
            Route::get('gallery/create', [ImageGalleryController::class, 'create'])->name('dashboard.gallery.create');
            Route::post('gallery/store', [ImageGalleryController::class, 'upload'])->name('dashboard.gallery.upload');
        });

        Route::middleware(['role:admin|staff'])->group(function() {
            Route::resource('order', OrderController::class); // order.index
            Route::get('food/{foodId}', [RecieptController::class, 'show'])->name('dashboard.receipt.food.show');
            // Route::get('order/list/all', [OrderController::class, 'listOrder'])->name('dashboard.order.table');
            // Route::get('receipt', [ReportController::class, 'receiptShow'])->name('order.receipt');
        });

        Route::middleware(['role:admin|manager|staff'])->group(function() {
            Route::get('profile', [DashboardController::class, 'myprofile'])->name('dashboard.profile');
            Route::resource('tariff', TariffController::class);
        });

    });

    require __DIR__ . '/auth.php';
